/*
 * "Hello World" example.
 *
 * This example prints 'Hello from Nios II' to the STDOUT stream. It runs on
 * the Nios II 'standard', 'full_featured', 'fast', and 'low_cost' example
 * designs. It runs with or without the MicroC/OS-II RTOS and requires a STDOUT
 * device in your system's hardware.
 * The memory footprint of this hosted application is ~69 kbytes by default
 * using the standard reference design.
 *
 * For a reduced footprint version of this template, and an explanation of how
 * to reduce the memory footprint for a given application, see the
 * "small_hello_world" template.
 *
 */

#include <stdio.h>
#include "scope_module.h"
#include "sig_gen_module.h"
#include "eemp_common.h"
#include "altera_avalon_pio_regs.h"
typedef struct
{
	bool oscilloscope;
	bool logic_analyzer;
}new_settings_tag;
static void rx_data_dispatch(uint32_t rx_data,new_settings_tag *new_settings)
{
	dispatch_tag dispatch_flag = rx_data>>24;
	switch(dispatch_flag)
	{
	case SCOPE_CFG_INDEX:
		new_settings->oscilloscope = true;
		scope_cfg_process(rx_data);
		break;
	case SIG_GEN_CFG_INDEX:
		sig_gen_cfg_process(rx_data);
		break;
	default:
		break;
	}
}

int main()
{
//  printf("Hello from Nios II!\n");
	scope_module_init();
	sig_gen_module_init();
	cy7c68013a_if_init();
	soc_ready();
	IOWR_ALTERA_AVALON_PIO_DIRECTION(PIO_0_BASE,0xffffffff);
	while(1)
	{
		new_settings_tag new_settings =
		{
			.oscilloscope = false,
			.logic_analyzer = false,
		};
	  //check ep8
		while(ep8_not_empty())
		{
			uint32_t rx_data = if_rx_start();
			rx_data_dispatch(rx_data,&new_settings);
		}
		if(new_settings.oscilloscope)
		{
			new_settings.oscilloscope = false;
			scope_start();
		}
		if(scope_fifo_full())
		{
			if_tx_scope();
			scope_start();
		}
		//TODO la_fifo_check then if_tx_la
	}
	return 0;
}
