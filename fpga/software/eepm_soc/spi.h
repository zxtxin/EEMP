/*
 * spi.h
 *
 *  Created on: 2016��3��27��
 *      Author: zxtxin
 */

#ifndef SPI_H_
#define SPI_H_
#include <stdint.h>
typedef enum{
	SCOPE_DAC_SPI = 0,
	SIG_GEN_DAC_SPI,
	SIG_GEN_DDS_SPI
}SPI_SLAVE_INDEX;
void spi_write_16bit(SPI_SLAVE_INDEX slave_select,uint16_t data);


#endif /* SPI_H_ */
