/*
 * scope_module.c
 *
 *  Created on: 2016��2��27��
 *      Author: Administrator
 */
#include <stddef.h>
#include <string.h>
#include <stdbool.h>
#include "cy7c68013a_if_module.h"
#include "spi.h"
#include "altera_avalon_pio_regs.h"
#include "system.h"
#include "sys/alt_irq.h"
#include "sys/alt_alarm.h"
#include "alt_types.h"

typedef struct
{
	volatile uint32_t scope_current_state;	//RO
	volatile uint32_t scope_config[2];		//WO
	volatile uint32_t scope_ctrl;			//WO
	volatile uint32_t scope_fifo_full;		//RO
	volatile uint32_t scope_reset;			//RW
	volatile uint32_t scope_clk_en;			//WO
}scope_reg_t;
#define SCOPE_MODULE ((scope_reg_t *)SCOPE_MODULE_0_BASE)
#define SCOPE_START_SAMPLE_CMD 		0x01
#define SCOPE_START_SCAN_CMD		0x02
typedef enum{
	scope_config_0 = 0,
	scope_config_1 ,
	scope_working_mode,
	sig_input_config,
	variable_gain_amp_config_0,
	variable_gain_amp_config_1,
	cha_offset_cfg,
	chb_offset_cfg,
}scope_cfg_tag;
enum{
	idle = 0,
	scan ,
	continuous,
	single
}op_mode;
static alt_alarm scan_alarm;

void scope_module_init()
{
	SCOPE_MODULE->scope_reset = 1;
	op_mode = idle;
}

static alt_u32 scan_alarm_callback(void *context)
{
	return 0;
}
static void set_scan_timer()
{
	alt_alarm_start(&scan_alarm,alt_ticks_per_second()>>10,scan_alarm_callback,NULL);
}
void scope_start()
{
	if(op_mode != idle)
	{
		if(op_mode == scan)
		{
			SCOPE_MODULE->scope_ctrl = SCOPE_START_SCAN_CMD;
			set_scan_timer();
		}
		else
		{
			SCOPE_MODULE->scope_ctrl = SCOPE_START_SAMPLE_CMD;
			if(op_mode == single)
			{
				op_mode = idle;
			}
		}
	}
}
static void scope_pio_set(uint16_t data)
{
	uint32_t io_val = IORD_ALTERA_AVALON_PIO_DATA(PIO_0_BASE);
	io_val &= ~0xffff;
	io_val |= data;
	IOWR_ALTERA_AVALON_PIO_DATA(PIO_0_BASE,io_val);
}
void scope_cfg_process(uint32_t rx)
{
	scope_cfg_tag flag = (rx>>16)&0xff;
	uint16_t payload = rx & 0xffff;
	SCOPE_MODULE->scope_reset = 1;
	while(SCOPE_MODULE->scope_reset == 1);
	switch(flag)
	{
	case scope_config_0:
		SCOPE_MODULE->scope_config[0] = payload;
		break;
	case scope_config_1:
		SCOPE_MODULE->scope_config[1] = payload;
		break;
	case scope_working_mode:
		op_mode = payload;
		break;
	case sig_input_config:
		scope_pio_set(payload);
		break;
	case variable_gain_amp_config_0:
	case variable_gain_amp_config_1:
	case cha_offset_cfg:
	case chb_offset_cfg:
		spi_write_16bit(SCOPE_DAC_SPI,payload);
		break;
	default:
		break;
	}
	SCOPE_MODULE->scope_clk_en = 1;
}
bool scope_fifo_full()
{
	return SCOPE_MODULE->scope_fifo_full;
}
