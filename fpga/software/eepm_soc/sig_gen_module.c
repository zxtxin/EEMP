/*
 * sig_gen.c
 *
 *  Created on: 2016��4��2��
 *      Author: zxtxin
 */
#include "sig_gen_module.h"
#include "spi.h"
#include "altera_avalon_pio_regs.h"
enum
{
	CONFIG_DDS_1 = 0,
	CONFIG_DDS_2,
	CONFIG_DDS_3,
	CONFIG_DDS_4,
	CONFIG_DDS_5,
	CONFIG_DAC,
	CONFIG_PIO,
};
void sig_gen_module_init()
{

}
static void sig_gen_pio_set(uint16_t data)
{
	uint32_t io_val = IORD_ALTERA_AVALON_PIO_DATA(PIO_0_BASE);
	io_val &= 0xffff;
	io_val |= data<<16;
	IOWR_ALTERA_AVALON_PIO_DATA(PIO_0_BASE,io_val);
}
void sig_gen_cfg_process(uint32_t rx)
{
	uint8_t label = rx >> 16;
	uint16_t payload = (uint16_t)rx;
	switch(label)
	{
	case CONFIG_DDS_1:
	case CONFIG_DDS_2:
	case CONFIG_DDS_3:
	case CONFIG_DDS_4:
	case CONFIG_DDS_5:
		spi_write_16bit(SIG_GEN_DDS_SPI,payload);
		break;
	case CONFIG_DAC:
		spi_write_16bit(SIG_GEN_DAC_SPI,payload);
		break;
	case CONFIG_PIO:
		sig_gen_pio_set(payload);
		break;
	default:
		break;
	}
}
