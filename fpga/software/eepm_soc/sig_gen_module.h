/*
 * sig_gen.h
 *
 *  Created on: 2016��4��2��
 *      Author: zxtxin
 */

#ifndef SIG_GEN_H_
#define SIG_GEN_H_
#include <stdint.h>
#include "cy7c68013a_if_module.h"
void sig_gen_module_init();
void sig_gen_cfg_process(uint32_t);
#endif /* SIG_GEN_H_ */
