#include "spi.h"
#include "altera_avalon_spi_regs.h"
#include "system.h"
void spi_write_16bit(SPI_SLAVE_INDEX slave_select,uint16_t data)
{
	uint32_t status;
	IOWR_ALTERA_AVALON_SPI_SLAVE_SEL(SPI_0_BASE, 1 << slave_select);
    do
    {
      status = IORD_ALTERA_AVALON_SPI_STATUS(SPI_0_BASE);
    }
    while (((status & ALTERA_AVALON_SPI_STATUS_TRDY_MSK) == 0) &&
            (status & ALTERA_AVALON_SPI_STATUS_RRDY_MSK) == 0);
    IOWR_ALTERA_AVALON_SPI_TXDATA(SPI_0_BASE, data);
    do
    {
      status = IORD_ALTERA_AVALON_SPI_STATUS(SPI_0_BASE);
    }
    while ((status & ALTERA_AVALON_SPI_STATUS_TMT_MSK) == 0);

}
