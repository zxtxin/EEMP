/*
 * scope_module.h
 *
 *  Created on: 2016��2��27��
 *      Author: Administrator
 */

#ifndef SCOPE_MODULE_H_
#define SCOPE_MODULE_H_
#include <stdint.h>
#include <stdbool.h>
#include "system.h"
#include "cy7c68013a_if_module.h"
void scope_module_init();
void scope_cfg_process(uint32_t);
void scope_start();
bool scope_fifo_full();
#endif /* SCOPE_MODULE_H_ */
