/*
 * cy7c68013a_if_module.h
 *
 *  Created on: 2016��2��27��
 *      Author: Administrator
 */

#ifndef CY7C68013A_IF_MODULE_H_
#define CY7C68013A_IF_MODULE_H_
#include <stdint.h>
#include <stdbool.h>
#include "system.h"

void cy7c68013a_if_init();
void soc_ready();
bool ep8_not_empty();
uint32_t if_rx_start();
void if_tx_scope();
#endif /* CY7C68013A_IF_MODULE_H_ */
