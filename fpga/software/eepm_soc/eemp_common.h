/*
 * eemp_common.h
 *
 *  Created on: 2016��4��30��
 *      Author: Administrator
 */

#ifndef EEMP_COMMON_H_
#define EEMP_COMMON_H_

#define SCOPE_CHA_TX_HEADER			(0xffff0001)
#define SCOPE_CHB_TX_HEADER			(0xffff0002)
#define LA_TX_HEADER				(0xffff0003)
typedef enum{
	SCOPE_CFG_INDEX = 1,
	SIG_GEN_CFG_INDEX = 2,
	LA_CFG_INDEX = 3,
}dispatch_tag;

#endif /* EEMP_COMMON_H_ */
