/*
 * cy7c68013a_if_module.c
 *
 *  Created on: 2016��2��27��
 *      Author: Administrator
 */
#include <stddef.h>
#include <string.h>
#include "cy7c68013a_if_module.h"
#include "eemp_common.h"
#include "sys/alt_irq.h"
typedef enum{
	DIRECTION_RX = 0,
	DIRECTION_TX
}IF_DIRECTION;
typedef enum{
	EP2_Full = 0,
	EP2_Not_Full
}FLAGB;
typedef enum{
	SOC_Ready = 0,
	SOC_Not_Ready
}SOC_READY_SIGNAL;
typedef struct{
	uint32_t flaga : 1;
	uint32_t flagb : 1;
	uint32_t flagc : 1;
	uint32_t :29;
}if_stat_t;
typedef struct{
	uint32_t ep8_not_empty : 1;
	uint32_t cha_fifo_rdempty : 1;
	uint32_t chb_fifo_rdempty : 1;
	uint32_t la_fifo_rdempty : 1;
	uint32_t :28;
}if_irq_t;
typedef struct
{
	volatile uint32_t if_irq_stat;			//RO
	volatile uint32_t if_irq_en;			//RW
	volatile uint32_t if_dir;				//WO
	volatile if_stat_t if_stat;				//RO
	volatile uint32_t if_fifo_sel;			//WO
	volatile uint32_t if_start_tx;			//WO
	volatile uint32_t if_packet_header;		//WO
	volatile uint32_t if_received_packet;	//RO
	volatile uint32_t if_start_rx;			//WO
	volatile if_irq_t if_irq_raw_stat;		//RO
	volatile uint32_t if_rx_busy;			//RO
	volatile uint32_t reserved;
	volatile uint32_t soc_ready;			//WO
}cy7c68013a_if_reg_t;
#define CY7C68013A_IF_MODULE ((cy7c68013a_if_reg_t *)CY7C68013A_IF_MODULE_0_BASE)

#define FIFO_SEL_CHANNEL_A			0x1
#define FIFO_SEL_CHANNEL_B			0x2
#define FIFO_SEL_LA					0x4
#define RX_PACKET_SIZE				4
#define NO_IRQ						0x00
#define EP8_NOT_EMPTY_IRQ 			0x01
#define CHA_FIFO_RDEMPTY_IRQ 		0x02
#define CHB_FIFO_RDEMPTY_IRQ		0x04
#define LA_FIFO_RDEMPTY_IRQ			0x08


static enum{
	if_idle,
	if_rx,
	if_tx_scope_cha,
	if_tx_scope_chb,
	if_tx_la
}if_stat;
static uint8_t irq_en;
#define DISABLE_IRQ_FLAG(IRQ)	irq_en &= ~(IRQ)
#define ENABLE_IRQ_FLAG(IRQ)		irq_en |= (IRQ)
#define SET_IRQ_PROPERTY()	CY7C68013A_IF_MODULE->if_irq_en = irq_en


uint32_t if_rx_start()
{
	if_stat = if_rx;
	CY7C68013A_IF_MODULE->if_dir = DIRECTION_RX;
	CY7C68013A_IF_MODULE->if_start_rx = 1;
	while(CY7C68013A_IF_MODULE->if_rx_busy == 1);
	if_stat = if_idle;
	return CY7C68013A_IF_MODULE->if_received_packet;
}

void cy7c68013a_if_init()
{
	if_stat = if_idle;
	CY7C68013A_IF_MODULE->if_dir = DIRECTION_RX;
//	ENABLE_IRQ_FLAG(CHB_FIFO_RDEMPTY_IRQ);
//	SET_IRQ_PROPERTY();

}

static void if_tx_start(uint32_t packet_header,uint32_t fifo_sel)
{
	CY7C68013A_IF_MODULE->if_dir = DIRECTION_TX;
	CY7C68013A_IF_MODULE->if_packet_header = packet_header;
	CY7C68013A_IF_MODULE->if_fifo_sel = fifo_sel;
	while(CY7C68013A_IF_MODULE->if_stat.flagb==EP2_Full);
	CY7C68013A_IF_MODULE->if_start_tx = 1;
}

void if_tx_scope()
{
	if_stat = if_tx_scope_cha;
	if_tx_start(SCOPE_CHA_TX_HEADER,FIFO_SEL_CHANNEL_A);
	while(CY7C68013A_IF_MODULE->if_irq_raw_stat.cha_fifo_rdempty == 0);
	if_stat = if_tx_scope_chb;
	if_tx_start(SCOPE_CHB_TX_HEADER,FIFO_SEL_CHANNEL_B);
	while(CY7C68013A_IF_MODULE->if_irq_raw_stat.chb_fifo_rdempty == 0);

}
void soc_ready()
{
	CY7C68013A_IF_MODULE->soc_ready = SOC_Ready;
}
bool ep8_not_empty()
{
	return CY7C68013A_IF_MODULE->if_stat.flaga;
}
