library ieee;
use ieee.std_logic_1164.all;
entity soc_top is
	port(
		dio	:   inout std_logic_vector(58 downto 0);
		
		dq : in std_logic_vector(15 downto 0);
		ddr_ad : out std_logic_vector(13 downto 0);
		ddr_cs_n: out std_logic;
		ddr_we_n: out std_logic;
		ddr_cas_n: out std_logic;
		ddr_ras_n: out std_logic;
		ddr_ba: out std_logic_vector(1 downto 0);
		ddr_ck: out std_logic;
		ddr_ck_n: out std_logic;
		ddr_cke: out std_logic;
		ddr_ldm: out std_logic;
		ddr_udm: out std_logic;
		ddr_ldqs: out std_logic;
		ddr_udqs: out std_logic;
		
		adc_clk_o   :    out std_logic;
		ADC_S2		:	 OUT STD_LOGIC;
		cha_data    :    in std_logic_vector(7 downto 0);
		chb_data    :    in std_logic_vector(7 downto 0);
		 
		dac_mclk_o : out std_logic;
		dac_sleep : out std_logic;
		dac_d : out std_logic_vector(9 downto 0 );
		
		sclk_spi: out std_logic;
		sync_dac_sig_gen: out std_logic;
		mosi_spi: out std_logic;
		sync_dac_scope: out std_logic;

		ACDC_1		:	 OUT STD_LOGIC;
		ACDC_2		:	 OUT STD_LOGIC;
		cha_sel     :    out std_logic;
		chb_sel     :    out std_logic;
		DDS_OUT_EN		:	 OUT STD_LOGIC;

		fd		:	 INOUT STD_LOGIC_VECTOR(15 DOWNTO 0);
		slrd		:	 OUT STD_LOGIC;
		slwr		:	 OUT STD_LOGIC;
		pktend		:	 OUT STD_LOGIC;
		fifoadr		:	 OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
		PA3:      in std_logic;
		sloe		:	 OUT STD_LOGIC;
		PA1:      out std_logic;
		PA0:      in std_logic;
		flagc    :   in std_logic;
		flagb		:	 IN STD_LOGIC;
		flaga		:	 IN STD_LOGIC;
		
		xtal_clk    :    in std_logic;
		ifclk		:	 IN STD_LOGIC;
		u_clockout:  in std_logic
	);
end soc_top;
architecture rtl of soc_top is

component nios2soc is
	port (
		clk_clk                                             : in    std_logic                     := 'X';             -- clk
		cy7c68013a_if_module_0_conduit_end_cha_fifo_data    : in    std_logic_vector(15 downto 0) := (others => 'X'); -- cha_fifo_data
		cy7c68013a_if_module_0_conduit_end_cha_fifo_rdempty : in    std_logic                     := 'X';             -- cha_fifo_rdempty
		cy7c68013a_if_module_0_conduit_end_cha_fifo_rdreq   : out   std_logic;                                        -- cha_fifo_rdreq
		cy7c68013a_if_module_0_conduit_end_chb_fifo_data    : in    std_logic_vector(15 downto 0) := (others => 'X'); -- chb_fifo_data
		cy7c68013a_if_module_0_conduit_end_chb_fifo_rdempty : in    std_logic                     := 'X';             -- chb_fifo_rdempty
		cy7c68013a_if_module_0_conduit_end_chb_fifo_rdreq   : out   std_logic;                                        -- chb_fifo_rdreq
		cy7c68013a_if_module_0_conduit_end_la_fifo_data     : in    std_logic_vector(15 downto 0) := (others => 'X'); -- la_fifo_data
		cy7c68013a_if_module_0_conduit_end_la_fifo_rdempty  : in    std_logic                     := 'X';             -- la_fifo_rdempty
		cy7c68013a_if_module_0_conduit_end_la_fifo_rdreq    : out   std_logic;                                        -- la_fifo_rdreq
		cy7c68013a_if_module_0_conduit_end_flaga_raw        : in    std_logic                     := 'X';             -- flaga_raw
		cy7c68013a_if_module_0_conduit_end_flagc_raw        : in    std_logic                     := 'X';             -- flagc_raw
		cy7c68013a_if_module_0_conduit_end_flagb_raw        : in    std_logic                     := 'X';             -- flagb_raw
		cy7c68013a_if_module_0_conduit_end_slwr             : out   std_logic;                                        -- slwr
		cy7c68013a_if_module_0_conduit_end_slrd             : out   std_logic;                                        -- slrd
		cy7c68013a_if_module_0_conduit_end_sloe             : out   std_logic;                                        -- sloe
		cy7c68013a_if_module_0_conduit_end_pktend           : out   std_logic;                                        -- pktend
		cy7c68013a_if_module_0_conduit_end_fifoadr          : out   std_logic_vector(1 downto 0);                     -- fifoadr
		cy7c68013a_if_module_0_conduit_end_fd               : inout std_logic_vector(15 downto 0) := (others => 'X'); -- fd
		cy7c68013a_if_module_0_conduit_end_soc_ready        : out   std_logic;                                        -- soc_ready
		pio_0_external_connection_export                    : out   std_logic_vector(31 downto 0);                    -- export
		reset_reset_n                                       : in    std_logic                     := 'X';             -- reset_n
		spi_0_external_MISO                                 : in    std_logic                     := 'X';             -- MISO
		spi_0_external_MOSI                                 : out   std_logic;                                        -- MOSI
		spi_0_external_SCLK                                 : out   std_logic;                                        -- SCLK
		spi_0_external_SS_n                                 : out   std_logic_vector(2 downto 0);                     -- SS_n
		scope_module_0_conduit_end_chb_fifo_data            : out   std_logic_vector(15 downto 0);                    -- chb_fifo_data
		scope_module_0_conduit_end_chb_fifo_rdempty         : out   std_logic;                                        -- chb_fifo_rdempty
		scope_module_0_conduit_end_chb_fifo_rdreq           : in    std_logic                     := 'X';             -- chb_fifo_rdreq
		scope_module_0_conduit_end_cha_fifo_data            : out   std_logic_vector(15 downto 0);                    -- cha_fifo_data
		scope_module_0_conduit_end_cha_fifo_rdempty         : out   std_logic;                                        -- cha_fifo_rdempty
		scope_module_0_conduit_end_cha_fifo_rdreq           : in    std_logic                     := 'X';             -- cha_fifo_rdreq
		scope_module_0_conduit_end_adc_clk                  : out   std_logic;                                        -- adc_clk
		scope_module_0_conduit_end_chb_data                 : in    std_logic_vector(7 downto 0)  := (others => 'X'); -- chb_data
		scope_module_0_conduit_end_cha_data                 : in    std_logic_vector(7 downto 0)  := (others => 'X')  -- cha_data
	);
end component;
component rst_ctrl is
	port (
		rst_in: in std_logic;
		clk: in std_logic;
		rst_out: out std_logic;
		nrst_out: out std_logic
	);
end component;
component ref_signal_gen is
    generic(khz:natural := 24000);
    port(clk:in std_logic;
         rst:in std_logic;
         sq_wave_1k: out std_logic);
end component;
signal cha_fifo_data,chb_fifo_data:std_logic_vector(15 downto 0);
signal cha_fifo_rdempty,chb_fifo_rdempty:std_logic;
signal cha_fifo_rdreq,chb_fifo_rdreq:std_logic;
signal pio:std_logic_vector(31 downto 0);
signal spi_sel:std_logic_vector(2 downto 0);
signal soc_ready:std_logic;
signal adc_clk: std_logic;
signal rst_in,rst_out_n:std_logic;
begin
    U2: ref_signal_gen port map
    (
        clk => xtal_clk,
        rst => rst_in,
        sq_wave_1k => dio(0)
    );
	adc_clk_o <= not adc_clk;
	dac_mclk_o <= adc_clk;
	PA1 <= soc_ready;
	rst_in <= PA0;
	sync_dac_scope <= spi_sel(0);
	sync_dac_sig_gen <= spi_sel(1);
	ACDC_1 <= pio(0);
	cha_sel <= pio(1);
	ACDC_2 <= pio(4);
	chb_sel <= pio(5);
	ADC_S2 <= '0';
	DDS_OUT_EN <= pio(16);
	soc: nios2soc port map(
		clk_clk                                              => ifclk,
		cy7c68013a_if_module_0_conduit_end_cha_fifo_data     => cha_fifo_data,
		cy7c68013a_if_module_0_conduit_end_cha_fifo_rdempty  => cha_fifo_rdempty,
		cy7c68013a_if_module_0_conduit_end_cha_fifo_rdreq    => cha_fifo_rdreq,
		cy7c68013a_if_module_0_conduit_end_chb_fifo_data     => chb_fifo_data,
		cy7c68013a_if_module_0_conduit_end_chb_fifo_rdempty  => chb_fifo_rdempty,
		cy7c68013a_if_module_0_conduit_end_chb_fifo_rdreq    => chb_fifo_rdreq,
		cy7c68013a_if_module_0_conduit_end_la_fifo_data      => (others => '0'),
		cy7c68013a_if_module_0_conduit_end_la_fifo_rdempty   => '0',
		cy7c68013a_if_module_0_conduit_end_la_fifo_rdreq     => open,
		cy7c68013a_if_module_0_conduit_end_flaga_raw         => flaga,
		cy7c68013a_if_module_0_conduit_end_flagb_raw         => flagb,
		cy7c68013a_if_module_0_conduit_end_flagc_raw         => flagc,
		cy7c68013a_if_module_0_conduit_end_slwr              => slwr,
		cy7c68013a_if_module_0_conduit_end_slrd              => slrd,
		cy7c68013a_if_module_0_conduit_end_sloe              => sloe,
		cy7c68013a_if_module_0_conduit_end_pktend            => pktend,
		cy7c68013a_if_module_0_conduit_end_fifoadr           => fifoadr,
		cy7c68013a_if_module_0_conduit_end_fd                => fd,
		pio_0_external_connection_export                     => pio,
		reset_reset_n                                        => rst_out_n,
		scope_module_0_conduit_end_cha_data                  => cha_data,
		scope_module_0_conduit_end_chb_data                  => chb_data,
		scope_module_0_conduit_end_adc_clk                   => adc_clk,
		scope_module_0_conduit_end_cha_fifo_rdreq            => cha_fifo_rdreq,
		scope_module_0_conduit_end_cha_fifo_rdempty          => cha_fifo_rdempty,
		scope_module_0_conduit_end_cha_fifo_data             => cha_fifo_data,
		scope_module_0_conduit_end_chb_fifo_rdreq            => chb_fifo_rdreq,
		scope_module_0_conduit_end_chb_fifo_rdempty          => chb_fifo_rdempty,
		scope_module_0_conduit_end_chb_fifo_data             => chb_fifo_data,
		spi_0_external_MISO                                  => '0',
		spi_0_external_MOSI                                  => mosi_spi,
		spi_0_external_SCLK                                  => sclk_spi,
		spi_0_external_SS_n                                  => spi_sel,
		cy7c68013a_if_module_0_conduit_end_soc_ready         => soc_ready
	);
	U1: rst_ctrl port map(
		rst_in => rst_in,
		clk => ifclk,
		rst_out => open,
		nrst_out => rst_out_n
	);
end rtl;