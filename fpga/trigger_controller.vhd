library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_signed.all;
entity trigger_controller is
	port ( cha_data: in std_logic_vector(7 downto 0);
			chb_data: in std_logic_vector(7 downto 0);
			trigger_source: in std_logic;
			trigger_voltage_level: in std_logic_vector(7 downto 0);
			mode:in std_logic_vector(1 downto 0);
			auto_trigger: in std_logic;
			trigger_enable:in std_logic;
			trigger_pulse: out std_logic;
			sys_clk: in std_logic;
			rst: in std_logic
	);
end trigger_controller;
architecture rtl of trigger_controller is
component positive_edge_trigger is
	port(
		ADC_DATA,REF_L,REF_H: in signed (7 downto 0);
		start,clk,rst: in std_logic;
		positive_edge_trigger_pulse: out std_logic	
	);
end component;
component negative_edge_trigger is
	port ( ADC_DATA, REF_L, REF_H : in signed (7 downto 0);
			start,clk ,rst: in std_logic;
			negative_edge_trigger_PULSE: out std_logic
			);
end component;
component force_trigger_counter is
	generic (count : natural := 8000);
	port (rst: in std_logic;
            start: in std_logic;
			clk: in std_logic;
			trigger_enable: in std_logic;
			force_trigger_pulse: out std_logic
			);
end component;
component rolling_average is
	generic (size: integer := 3;
			width:integer:= 8);
	port (
		input_data: in signed (width-1 downto 0);
		output_data: out signed (width-1 downto 0);
		valid: out std_logic;
		clk: in std_logic;
		rst: in std_logic	
	);
end component;
signal trigger_data: std_logic_vector(7 downto 0);
signal filtered_data: signed(7 downto 0);
signal pulse:std_logic;
signal positive_edge_trigger_pulse_signal,negative_edge_trigger_PULSE_signal,force_trigger_pulse_signal:std_logic;
signal trigger_en_prev:std_logic;
signal trigger_start,posedge_start,negedge_start,force_trigger_start:std_logic;
begin
	U3: positive_edge_trigger port map (filtered_data,signed(trigger_voltage_level)-7,signed(trigger_voltage_level)+7,posedge_start,sys_clk,rst,positive_edge_trigger_pulse_signal);
	U1: negative_edge_trigger port map (filtered_data,signed(trigger_voltage_level)-7,signed(trigger_voltage_level)+7,negedge_start,sys_clk,rst,negative_edge_trigger_PULSE_signal);
	U2: force_trigger_counter port map (rst,force_trigger_start,sys_clk,trigger_enable,force_trigger_pulse_signal);
	--U4: rolling_average port map(signed(trigger_data),filtered_data,open,sys_clk,rst);
	filtered_data <= signed(trigger_data);
	with trigger_source select
		trigger_data <= cha_data when '0',
						chb_data when '1',
						(others => 'X') when others;
    process(sys_clk,rst)
    begin
        if(rst = '1') then
            trigger_pulse <= '0';
            trigger_en_prev <= '0';
            trigger_start <= '0';
        elsif(sys_clk = '1' and sys_clk'event) then
            trigger_en_prev <= trigger_enable;
            if(trigger_en_prev = '0' and trigger_enable = '1') then
                trigger_start <= '1';
			else
				trigger_start <= '0';
            end if;
            if(pulse = '1' or force_trigger_pulse_signal = '1') then
                trigger_pulse <= '1';
			else
				trigger_pulse <= '0';
            end if;
        end if;
    end process;
    force_trigger_start <= trigger_start and auto_trigger;
    posedge_start <= trigger_start when mode = "00" else '0';
    negedge_start <= trigger_start when mode = "01" else '0';
	process(mode,negative_edge_trigger_PULSE_signal,positive_edge_trigger_pulse_signal)
	begin
		case mode is
			when "00" =>
				pulse <= positive_edge_trigger_pulse_signal;
			when "01" =>
				pulse <= negative_edge_trigger_PULSE_signal;
			when "10" =>
				pulse <= '0';
			when "11" =>
				pulse <= '0';
		end case;
	end process;
end rtl;