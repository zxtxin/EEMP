library ieee;
use ieee.std_logic_1164.all;

entity ref_signal_gen_tb is
end ref_signal_gen_tb;
architecture tb of ref_signal_gen_tb is
component ref_signal_gen is
    generic (khz:natural:= 24000);
    port( clk:in std_logic;
          rst:in std_logic;
          sq_wave_1k: out std_logic
          );
end component;
signal xtal_clk: std_logic := '0';
signal rst_in:std_logic;
signal ref_wave:std_logic;
constant period:time:= 41.66 ns;
begin
    U1: ref_signal_gen port map
    (
        clk => xtal_clk,
        rst => rst_in,
        sq_wave_1k => ref_wave
    );
    process(xtal_clk)
    begin
        xtal_clk <= not xtal_clk after period/2;
    end process;
    rst_in <= '1',
            '0' after 10 ns;
end tb;
        
        