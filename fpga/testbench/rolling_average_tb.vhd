library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
entity rolling_average_tb is
end rolling_average_tb;
architecture tb of rolling_average_tb is
component rolling_average is
	generic (size: integer := 3;
			width:integer:= 8);
	port (
		input_data: in signed (7 downto 0);
		output_data: out signed (7 downto 0);
		valid: out std_logic;
		clk: in std_logic;
		rst: in std_logic	
	);
end component;
signal input: signed(7 downto 0);
signal output: signed(7 downto 0);
signal clk:std_logic:= '0';
signal rst:std_logic;
signal data_valid:std_logic;
constant PERIOD:time:= 10 ns;
begin
	U1: rolling_average port map
	(
		input_data => input,
		output_data => output,
		valid => data_valid,
		clk => clk,
		rst => rst	
	);
	process(clk)
	begin
		clk <= not clk after PERIOD /2;
	end process;
	rst <= '1',
			'0' after 2 ns;
	process
	begin
		input <= conv_signed(8,8);
		wait for PERIOD;
		input <= conv_signed(7,8);
		wait for PERIOD;
		input <= conv_signed(6,8);
		wait for PERIOD;
		input <= conv_signed(5,8);
		wait for PERIOD;
		input <= conv_signed(4,8);
		wait for PERIOD;
		input <= conv_signed(3,8);
		wait for PERIOD;
		input <= conv_signed(2,8);
		wait for PERIOD;
		input <= conv_signed(1,8);
		wait for PERIOD;
		input <= conv_signed(-1,8);
		wait for PERIOD;
		input <= conv_signed(-3,8);
		wait for PERIOD;
		input <= conv_signed(-5,8);
		wait for PERIOD;
		input <= conv_signed(-7,8);
		wait for PERIOD;
		input <= conv_signed(-9,8);
		wait for PERIOD;
		input <= conv_signed(-11,8);
		wait for PERIOD;
		input <= conv_signed(-13,8);
		wait for PERIOD;
		input <= conv_signed(-15,8);
		wait for PERIOD;
		input <= conv_signed(-17,8);
		wait for PERIOD;
	end process;




end tb;