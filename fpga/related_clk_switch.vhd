library ieee;
use ieee.std_logic_1164.all;
entity related_clk_switch is
    port (
        out_clk: out std_logic;
        clk_0: in std_logic;
        clk_1: in std_logic;
        sel: in std_logic    
    );
end related_clk_switch;
architecture rtl of related_clk_switch is
signal clk0_ff,clk1_ff:std_logic;
begin
    process(clk_0)
    begin
        if(clk_0 = '0' and clk_0'event) then
            clk0_ff <= sel and not clk1_ff;
        end if;
    end process;
    process(clk_1)
    begin
        if(clk_1 = '0' and clk_1'event) then
            clk1_ff <= not sel and not clk0_ff;
        end if;
    end process;
    out_clk <= (clk_1 and clk1_ff) or (clk_0 and clk0_ff);
end rtl;