library ieee;
use ieee.std_logic_1164.all;
entity unrelated_clk_switch is
    port (
        out_clk: out std_logic;
        clk_0: in std_logic;
        clk_1: in std_logic;
        sel: in std_logic    
    );
end unrelated_clk_switch;
architecture rtl of unrelated_clk_switch is
signal clk0_ff0,clk0_ff1:std_logic;
signal clk1_ff0,clk1_ff1:std_logic;
begin
    process(clk_1)
    begin
        if(clk_1 = '1' and clk_1'event) then
            clk1_ff0 <= (not clk0_ff1) and sel;
        end if;
        if(clk_1 = '0' and clk_1'event) then
            clk1_ff1 <= clk1_ff0;
        end if;
    end process;
    process(clk_0)
    begin
        if(clk_0 = '1' and clk_0'event) then
            clk0_ff0 <= (not clk1_ff1) and (not sel);
        end if;
        if(clk_0 = '0' and clk_0'event) then
            clk0_ff1 <= clk0_ff0;
        end if;
    end process;
    out_clk <= (clk1_ff1 and clk_1) or (clk0_ff1 and clk_0);
end rtl;