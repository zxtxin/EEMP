library ieee;
use ieee.std_logic_1164.all;
entity ref_signal_gen is
    generic (khz: natural);
    port( clk:in std_logic;
          rst:in std_logic;
          sq_wave_1k: out std_logic
          );
end ref_signal_gen;
architecture arch_ref_signal_gen of ref_signal_gen is
signal counter:natural;
signal sig_out:std_logic;
begin
    process(clk,rst)
    begin
        if(rst = '1')then
            counter <= 0;
            sig_out <= '0';
        elsif(clk'event and clk = '1')then
            if(counter = khz/2 - 1) then
                counter <= 0;
                sig_out <= not sig_out;
            else
                counter <= counter + 1;
                sig_out <= sig_out;
            end if;
        end if;
    end process;
    sq_wave_1k <= sig_out;
end arch_ref_signal_gen;
