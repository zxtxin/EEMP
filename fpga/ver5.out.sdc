## Generated SDC file "ver5.out.sdc"

## Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, the Altera Quartus II License Agreement,
## the Altera MegaCore Function License Agreement, or other 
## applicable license agreement, including, without limitation, 
## that your use is for the sole purpose of programming logic 
## devices manufactured by Altera and sold by Altera or its 
## authorized distributors.  Please refer to the applicable 
## agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Full Version"

## DATE    "Sun Sep 03 20:46:15 2017"

##
## DEVICE  "EP4CE10E22C8"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {altera_reserved_tck} -period 100.000 -waveform { 0.000 50.000 } [get_ports {altera_reserved_tck}]
create_clock -name {ifclk} -period 33.333 -waveform { 0.000 16.667 } [get_ports {ifclk}]


#**************************************************************
# Create Generated Clock
#**************************************************************

create_generated_clock -name {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]} -source [get_pins {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|inclk[0]}] -duty_cycle 50.000 -multiply_by 20 -divide_by 3 -master_clock {ifclk} [get_pins {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]}] 
create_generated_clock -name {adc_clk} -source [get_registers {nios2soc:soc|scope_module:scope_module_0|clk_gen:U1|clk_100mhz}] -master_clock {clk_100mhz} [get_ports {adc_clk_o}] 
create_generated_clock -name {scope_clk} -source [get_pins {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]}] -divide_by 2 -master_clock {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]} [get_registers {nios2soc:soc|scope_module:scope_module_0|clk_gen:U1|div_clk}] 
create_generated_clock -name {double_period_scope_clk} -source [get_registers {nios2soc:soc|scope_module:scope_module_0|clk_gen:U1|div_clk}] -divide_by 2 -master_clock {scope_clk} [get_registers {nios2soc:soc|scope_module:scope_module_0|scope_core:U2|fifo_rdclk_gen:U3|double_period_scope_clk}] 
create_generated_clock -name {double_period_scope_clk_mux} -source [get_registers {nios2soc:soc|scope_module:scope_module_0|scope_core:U2|fifo_rdclk_gen:U3|double_period_scope_clk}] -master_clock {double_period_scope_clk} [get_nets {soc|scope_module_0|U2|U3|U1|out_clk}] -add
create_generated_clock -name {ifclk_mux} -source [get_ports {ifclk}] -master_clock {ifclk} [get_nets {soc|scope_module_0|U2|U3|U1|out_clk}] -add
create_generated_clock -name {clk_100mhz} -source [get_pins {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]}] -divide_by 2 -master_clock {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]} [get_registers {nios2soc:soc|scope_module:scope_module_0|clk_gen:U1|clk_100mhz}] 


#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************

set_clock_uncertainty -rise_from [get_clocks {clk_100mhz}] -rise_to [get_clocks {clk_100mhz}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {clk_100mhz}] -fall_to [get_clocks {clk_100mhz}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {clk_100mhz}] -rise_to [get_clocks {scope_clk}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {clk_100mhz}] -fall_to [get_clocks {scope_clk}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {clk_100mhz}] -rise_to [get_clocks {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {clk_100mhz}] -fall_to [get_clocks {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {clk_100mhz}] -rise_to [get_clocks {clk_100mhz}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {clk_100mhz}] -fall_to [get_clocks {clk_100mhz}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {clk_100mhz}] -rise_to [get_clocks {scope_clk}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {clk_100mhz}] -fall_to [get_clocks {scope_clk}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {clk_100mhz}] -rise_to [get_clocks {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {clk_100mhz}] -fall_to [get_clocks {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {ifclk_mux}] -rise_to [get_clocks {ifclk_mux}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {ifclk_mux}] -fall_to [get_clocks {ifclk_mux}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {ifclk_mux}] -rise_to [get_clocks {ifclk}] -setup 0.100  
set_clock_uncertainty -rise_from [get_clocks {ifclk_mux}] -rise_to [get_clocks {ifclk}] -hold 0.070  
set_clock_uncertainty -rise_from [get_clocks {ifclk_mux}] -fall_to [get_clocks {ifclk}] -setup 0.100  
set_clock_uncertainty -rise_from [get_clocks {ifclk_mux}] -fall_to [get_clocks {ifclk}] -hold 0.070  
set_clock_uncertainty -fall_from [get_clocks {ifclk_mux}] -rise_to [get_clocks {ifclk_mux}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {ifclk_mux}] -fall_to [get_clocks {ifclk_mux}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {ifclk_mux}] -rise_to [get_clocks {ifclk}] -setup 0.100  
set_clock_uncertainty -fall_from [get_clocks {ifclk_mux}] -rise_to [get_clocks {ifclk}] -hold 0.070  
set_clock_uncertainty -fall_from [get_clocks {ifclk_mux}] -fall_to [get_clocks {ifclk}] -setup 0.100  
set_clock_uncertainty -fall_from [get_clocks {ifclk_mux}] -fall_to [get_clocks {ifclk}] -hold 0.070  
set_clock_uncertainty -rise_from [get_clocks {double_period_scope_clk_mux}] -rise_to [get_clocks {double_period_scope_clk_mux}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {double_period_scope_clk_mux}] -fall_to [get_clocks {double_period_scope_clk_mux}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {double_period_scope_clk_mux}] -rise_to [get_clocks {scope_clk}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {double_period_scope_clk_mux}] -fall_to [get_clocks {scope_clk}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {double_period_scope_clk_mux}] -rise_to [get_clocks {double_period_scope_clk_mux}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {double_period_scope_clk_mux}] -fall_to [get_clocks {double_period_scope_clk_mux}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {double_period_scope_clk_mux}] -rise_to [get_clocks {scope_clk}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {double_period_scope_clk_mux}] -fall_to [get_clocks {scope_clk}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {double_period_scope_clk}] -rise_to [get_clocks {double_period_scope_clk}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {double_period_scope_clk}] -fall_to [get_clocks {double_period_scope_clk}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {double_period_scope_clk}] -rise_to [get_clocks {scope_clk}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {double_period_scope_clk}] -fall_to [get_clocks {scope_clk}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {double_period_scope_clk}] -rise_to [get_clocks {double_period_scope_clk}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {double_period_scope_clk}] -fall_to [get_clocks {double_period_scope_clk}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {double_period_scope_clk}] -rise_to [get_clocks {scope_clk}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {double_period_scope_clk}] -fall_to [get_clocks {scope_clk}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {scope_clk}] -rise_to [get_clocks {clk_100mhz}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {scope_clk}] -fall_to [get_clocks {clk_100mhz}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {scope_clk}] -rise_to [get_clocks {double_period_scope_clk_mux}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {scope_clk}] -fall_to [get_clocks {double_period_scope_clk_mux}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {scope_clk}] -rise_to [get_clocks {double_period_scope_clk}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {scope_clk}] -fall_to [get_clocks {double_period_scope_clk}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {scope_clk}] -rise_to [get_clocks {scope_clk}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {scope_clk}] -fall_to [get_clocks {scope_clk}]  0.030  
set_clock_uncertainty -rise_from [get_clocks {scope_clk}] -rise_to [get_clocks {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {scope_clk}] -fall_to [get_clocks {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {scope_clk}] -rise_to [get_clocks {clk_100mhz}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {scope_clk}] -fall_to [get_clocks {clk_100mhz}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {scope_clk}] -rise_to [get_clocks {double_period_scope_clk_mux}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {scope_clk}] -fall_to [get_clocks {double_period_scope_clk_mux}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {scope_clk}] -rise_to [get_clocks {double_period_scope_clk}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {scope_clk}] -fall_to [get_clocks {double_period_scope_clk}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {scope_clk}] -rise_to [get_clocks {scope_clk}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {scope_clk}] -fall_to [get_clocks {scope_clk}]  0.030  
set_clock_uncertainty -fall_from [get_clocks {scope_clk}] -rise_to [get_clocks {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {scope_clk}] -fall_to [get_clocks {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {adc_clk}] -rise_to [get_clocks {clk_100mhz}]  0.110  
set_clock_uncertainty -rise_from [get_clocks {adc_clk}] -fall_to [get_clocks {clk_100mhz}]  0.110  
set_clock_uncertainty -fall_from [get_clocks {adc_clk}] -rise_to [get_clocks {clk_100mhz}]  0.110  
set_clock_uncertainty -fall_from [get_clocks {adc_clk}] -fall_to [get_clocks {clk_100mhz}]  0.110  
set_clock_uncertainty -rise_from [get_clocks {ifclk}] -rise_to [get_clocks {ifclk_mux}] -setup 0.070  
set_clock_uncertainty -rise_from [get_clocks {ifclk}] -rise_to [get_clocks {ifclk_mux}] -hold 0.100  
set_clock_uncertainty -rise_from [get_clocks {ifclk}] -fall_to [get_clocks {ifclk_mux}] -setup 0.070  
set_clock_uncertainty -rise_from [get_clocks {ifclk}] -fall_to [get_clocks {ifclk_mux}] -hold 0.100  
set_clock_uncertainty -rise_from [get_clocks {ifclk}] -rise_to [get_clocks {ifclk}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {ifclk}] -fall_to [get_clocks {ifclk}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {ifclk}] -rise_to [get_clocks {ifclk_mux}] -setup 0.070  
set_clock_uncertainty -fall_from [get_clocks {ifclk}] -rise_to [get_clocks {ifclk_mux}] -hold 0.100  
set_clock_uncertainty -fall_from [get_clocks {ifclk}] -fall_to [get_clocks {ifclk_mux}] -setup 0.070  
set_clock_uncertainty -fall_from [get_clocks {ifclk}] -fall_to [get_clocks {ifclk_mux}] -hold 0.100  
set_clock_uncertainty -fall_from [get_clocks {ifclk}] -rise_to [get_clocks {ifclk}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {ifclk}] -fall_to [get_clocks {ifclk}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {scope_clk}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {scope_clk}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {scope_clk}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {scope_clk}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {altera_reserved_tck}] -rise_to [get_clocks {altera_reserved_tck}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {altera_reserved_tck}] -fall_to [get_clocks {altera_reserved_tck}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {altera_reserved_tck}] -rise_to [get_clocks {altera_reserved_tck}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {altera_reserved_tck}] -fall_to [get_clocks {altera_reserved_tck}]  0.020  


#**************************************************************
# Set Input Delay
#**************************************************************

set_input_delay -add_delay -max -clock_fall -clock [get_clocks {adc_clk}]  1.000 [get_ports {cha_data[0]}]
set_input_delay -add_delay -min -clock_fall -clock [get_clocks {adc_clk}]  -0.500 [get_ports {cha_data[0]}]
set_input_delay -add_delay -max -clock_fall -clock [get_clocks {adc_clk}]  1.000 [get_ports {cha_data[1]}]
set_input_delay -add_delay -min -clock_fall -clock [get_clocks {adc_clk}]  -0.500 [get_ports {cha_data[1]}]
set_input_delay -add_delay -max -clock_fall -clock [get_clocks {adc_clk}]  1.000 [get_ports {cha_data[2]}]
set_input_delay -add_delay -min -clock_fall -clock [get_clocks {adc_clk}]  -0.500 [get_ports {cha_data[2]}]
set_input_delay -add_delay -max -clock_fall -clock [get_clocks {adc_clk}]  1.000 [get_ports {cha_data[3]}]
set_input_delay -add_delay -min -clock_fall -clock [get_clocks {adc_clk}]  -0.500 [get_ports {cha_data[3]}]
set_input_delay -add_delay -max -clock_fall -clock [get_clocks {adc_clk}]  1.000 [get_ports {cha_data[4]}]
set_input_delay -add_delay -min -clock_fall -clock [get_clocks {adc_clk}]  -0.500 [get_ports {cha_data[4]}]
set_input_delay -add_delay -max -clock_fall -clock [get_clocks {adc_clk}]  1.000 [get_ports {cha_data[5]}]
set_input_delay -add_delay -min -clock_fall -clock [get_clocks {adc_clk}]  -0.500 [get_ports {cha_data[5]}]
set_input_delay -add_delay -max -clock_fall -clock [get_clocks {adc_clk}]  1.000 [get_ports {cha_data[6]}]
set_input_delay -add_delay -min -clock_fall -clock [get_clocks {adc_clk}]  -0.500 [get_ports {cha_data[6]}]
set_input_delay -add_delay -max -clock_fall -clock [get_clocks {adc_clk}]  1.000 [get_ports {cha_data[7]}]
set_input_delay -add_delay -min -clock_fall -clock [get_clocks {adc_clk}]  -0.500 [get_ports {cha_data[7]}]
set_input_delay -add_delay -max -clock_fall -clock [get_clocks {adc_clk}]  1.000 [get_ports {chb_data[0]}]
set_input_delay -add_delay -min -clock_fall -clock [get_clocks {adc_clk}]  -0.500 [get_ports {chb_data[0]}]
set_input_delay -add_delay -max -clock_fall -clock [get_clocks {adc_clk}]  1.000 [get_ports {chb_data[1]}]
set_input_delay -add_delay -min -clock_fall -clock [get_clocks {adc_clk}]  -0.500 [get_ports {chb_data[1]}]
set_input_delay -add_delay -max -clock_fall -clock [get_clocks {adc_clk}]  1.000 [get_ports {chb_data[2]}]
set_input_delay -add_delay -min -clock_fall -clock [get_clocks {adc_clk}]  -0.500 [get_ports {chb_data[2]}]
set_input_delay -add_delay -max -clock_fall -clock [get_clocks {adc_clk}]  1.000 [get_ports {chb_data[3]}]
set_input_delay -add_delay -min -clock_fall -clock [get_clocks {adc_clk}]  -0.500 [get_ports {chb_data[3]}]
set_input_delay -add_delay -max -clock_fall -clock [get_clocks {adc_clk}]  1.000 [get_ports {chb_data[4]}]
set_input_delay -add_delay -min -clock_fall -clock [get_clocks {adc_clk}]  -0.500 [get_ports {chb_data[4]}]
set_input_delay -add_delay -max -clock_fall -clock [get_clocks {adc_clk}]  1.000 [get_ports {chb_data[5]}]
set_input_delay -add_delay -min -clock_fall -clock [get_clocks {adc_clk}]  -0.500 [get_ports {chb_data[5]}]
set_input_delay -add_delay -max -clock_fall -clock [get_clocks {adc_clk}]  1.000 [get_ports {chb_data[6]}]
set_input_delay -add_delay -min -clock_fall -clock [get_clocks {adc_clk}]  -0.500 [get_ports {chb_data[6]}]
set_input_delay -add_delay -max -clock_fall -clock [get_clocks {adc_clk}]  1.000 [get_ports {chb_data[7]}]
set_input_delay -add_delay -min -clock_fall -clock [get_clocks {adc_clk}]  -0.500 [get_ports {chb_data[7]}]
set_input_delay -add_delay -max -clock [get_clocks {ifclk}]  11.000 [get_ports {fd[0]}]
set_input_delay -add_delay -min -clock [get_clocks {ifclk}]  5.000 [get_ports {fd[0]}]
set_input_delay -add_delay -max -clock [get_clocks {ifclk}]  11.000 [get_ports {fd[1]}]
set_input_delay -add_delay -min -clock [get_clocks {ifclk}]  5.000 [get_ports {fd[1]}]
set_input_delay -add_delay -max -clock [get_clocks {ifclk}]  11.000 [get_ports {fd[2]}]
set_input_delay -add_delay -min -clock [get_clocks {ifclk}]  5.000 [get_ports {fd[2]}]
set_input_delay -add_delay -max -clock [get_clocks {ifclk}]  11.000 [get_ports {fd[3]}]
set_input_delay -add_delay -min -clock [get_clocks {ifclk}]  5.000 [get_ports {fd[3]}]
set_input_delay -add_delay -max -clock [get_clocks {ifclk}]  11.000 [get_ports {fd[4]}]
set_input_delay -add_delay -min -clock [get_clocks {ifclk}]  5.000 [get_ports {fd[4]}]
set_input_delay -add_delay -max -clock [get_clocks {ifclk}]  11.000 [get_ports {fd[5]}]
set_input_delay -add_delay -min -clock [get_clocks {ifclk}]  5.000 [get_ports {fd[5]}]
set_input_delay -add_delay -max -clock [get_clocks {ifclk}]  11.000 [get_ports {fd[6]}]
set_input_delay -add_delay -min -clock [get_clocks {ifclk}]  5.000 [get_ports {fd[6]}]
set_input_delay -add_delay -max -clock [get_clocks {ifclk}]  11.000 [get_ports {fd[7]}]
set_input_delay -add_delay -min -clock [get_clocks {ifclk}]  5.000 [get_ports {fd[7]}]
set_input_delay -add_delay -max -clock [get_clocks {ifclk}]  11.000 [get_ports {fd[8]}]
set_input_delay -add_delay -min -clock [get_clocks {ifclk}]  5.000 [get_ports {fd[8]}]
set_input_delay -add_delay -max -clock [get_clocks {ifclk}]  11.000 [get_ports {fd[9]}]
set_input_delay -add_delay -min -clock [get_clocks {ifclk}]  5.000 [get_ports {fd[9]}]
set_input_delay -add_delay -max -clock [get_clocks {ifclk}]  11.000 [get_ports {fd[10]}]
set_input_delay -add_delay -min -clock [get_clocks {ifclk}]  5.000 [get_ports {fd[10]}]
set_input_delay -add_delay -max -clock [get_clocks {ifclk}]  11.000 [get_ports {fd[11]}]
set_input_delay -add_delay -min -clock [get_clocks {ifclk}]  5.000 [get_ports {fd[11]}]
set_input_delay -add_delay -max -clock [get_clocks {ifclk}]  11.000 [get_ports {fd[12]}]
set_input_delay -add_delay -min -clock [get_clocks {ifclk}]  5.000 [get_ports {fd[12]}]
set_input_delay -add_delay -max -clock [get_clocks {ifclk}]  11.000 [get_ports {fd[13]}]
set_input_delay -add_delay -min -clock [get_clocks {ifclk}]  5.000 [get_ports {fd[13]}]
set_input_delay -add_delay -max -clock [get_clocks {ifclk}]  11.000 [get_ports {fd[14]}]
set_input_delay -add_delay -min -clock [get_clocks {ifclk}]  5.000 [get_ports {fd[14]}]
set_input_delay -add_delay -max -clock [get_clocks {ifclk}]  11.000 [get_ports {fd[15]}]
set_input_delay -add_delay -min -clock [get_clocks {ifclk}]  5.000 [get_ports {fd[15]}]
set_input_delay -add_delay -max -clock [get_clocks {ifclk}]  9.500 [get_ports {flaga}]
set_input_delay -add_delay -min -clock [get_clocks {ifclk}]  0.300 [get_ports {flaga}]
set_input_delay -add_delay -max -clock [get_clocks {ifclk}]  9.500 [get_ports {flagb}]
set_input_delay -add_delay -min -clock [get_clocks {ifclk}]  0.300 [get_ports {flagb}]
set_input_delay -add_delay -max -clock [get_clocks {ifclk}]  9.500 [get_ports {flagc}]
set_input_delay -add_delay -min -clock [get_clocks {ifclk}]  0.300 [get_ports {flagc}]


#**************************************************************
# Set Output Delay
#**************************************************************

set_output_delay -add_delay  -clock [get_clocks {ifclk}]  0.300 [get_ports {ACDC_1}]
set_output_delay -add_delay  -clock [get_clocks {ifclk}]  0.300 [get_ports {ACDC_2}]
set_output_delay -add_delay  -clock [get_clocks {ifclk}]  0.300 [get_ports {DDS_OUT_EN}]
set_output_delay -add_delay  -clock [get_clocks {ifclk}]  0.300 [get_ports {PA1}]
set_output_delay -add_delay -max -clock [get_clocks {ifclk}]  9.200 [get_ports {fd[0]}]
set_output_delay -add_delay -min -clock [get_clocks {ifclk}]  0.300 [get_ports {fd[0]}]
set_output_delay -add_delay -max -clock [get_clocks {ifclk}]  9.200 [get_ports {fd[1]}]
set_output_delay -add_delay -min -clock [get_clocks {ifclk}]  0.300 [get_ports {fd[1]}]
set_output_delay -add_delay -max -clock [get_clocks {ifclk}]  9.200 [get_ports {fd[2]}]
set_output_delay -add_delay -min -clock [get_clocks {ifclk}]  0.300 [get_ports {fd[2]}]
set_output_delay -add_delay -max -clock [get_clocks {ifclk}]  9.200 [get_ports {fd[3]}]
set_output_delay -add_delay -min -clock [get_clocks {ifclk}]  0.300 [get_ports {fd[3]}]
set_output_delay -add_delay -max -clock [get_clocks {ifclk}]  9.200 [get_ports {fd[4]}]
set_output_delay -add_delay -min -clock [get_clocks {ifclk}]  0.300 [get_ports {fd[4]}]
set_output_delay -add_delay -max -clock [get_clocks {ifclk}]  9.200 [get_ports {fd[5]}]
set_output_delay -add_delay -min -clock [get_clocks {ifclk}]  0.300 [get_ports {fd[5]}]
set_output_delay -add_delay -max -clock [get_clocks {ifclk}]  9.200 [get_ports {fd[6]}]
set_output_delay -add_delay -min -clock [get_clocks {ifclk}]  0.300 [get_ports {fd[6]}]
set_output_delay -add_delay -max -clock [get_clocks {ifclk}]  9.200 [get_ports {fd[7]}]
set_output_delay -add_delay -min -clock [get_clocks {ifclk}]  0.300 [get_ports {fd[7]}]
set_output_delay -add_delay -max -clock [get_clocks {ifclk}]  9.200 [get_ports {fd[8]}]
set_output_delay -add_delay -min -clock [get_clocks {ifclk}]  0.300 [get_ports {fd[8]}]
set_output_delay -add_delay -max -clock [get_clocks {ifclk}]  9.200 [get_ports {fd[9]}]
set_output_delay -add_delay -min -clock [get_clocks {ifclk}]  0.300 [get_ports {fd[9]}]
set_output_delay -add_delay -max -clock [get_clocks {ifclk}]  9.200 [get_ports {fd[10]}]
set_output_delay -add_delay -min -clock [get_clocks {ifclk}]  0.300 [get_ports {fd[10]}]
set_output_delay -add_delay -max -clock [get_clocks {ifclk}]  9.200 [get_ports {fd[11]}]
set_output_delay -add_delay -min -clock [get_clocks {ifclk}]  0.300 [get_ports {fd[11]}]
set_output_delay -add_delay -max -clock [get_clocks {ifclk}]  9.200 [get_ports {fd[12]}]
set_output_delay -add_delay -min -clock [get_clocks {ifclk}]  0.300 [get_ports {fd[12]}]
set_output_delay -add_delay -max -clock [get_clocks {ifclk}]  9.200 [get_ports {fd[13]}]
set_output_delay -add_delay -min -clock [get_clocks {ifclk}]  0.300 [get_ports {fd[13]}]
set_output_delay -add_delay -max -clock [get_clocks {ifclk}]  9.200 [get_ports {fd[14]}]
set_output_delay -add_delay -min -clock [get_clocks {ifclk}]  0.300 [get_ports {fd[14]}]
set_output_delay -add_delay -max -clock [get_clocks {ifclk}]  9.200 [get_ports {fd[15]}]
set_output_delay -add_delay -min -clock [get_clocks {ifclk}]  0.300 [get_ports {fd[15]}]
set_output_delay -add_delay -max -clock [get_clocks {ifclk}]  4.170 [get_ports {fifoadr[0]}]
set_output_delay -add_delay -min -clock [get_clocks {ifclk}]  0.300 [get_ports {fifoadr[0]}]
set_output_delay -add_delay -max -clock [get_clocks {ifclk}]  4.170 [get_ports {fifoadr[1]}]
set_output_delay -add_delay -min -clock [get_clocks {ifclk}]  0.300 [get_ports {fifoadr[1]}]
set_output_delay -add_delay  -clock [get_clocks {ifclk}]  0.300 [get_ports {led}]
set_output_delay -add_delay -max -clock [get_clocks {ifclk}]  14.600 [get_ports {pktend}]
set_output_delay -add_delay -min -clock [get_clocks {ifclk}]  0.300 [get_ports {pktend}]
set_output_delay -add_delay -max -clock [get_clocks {ifclk}]  10.500 [get_ports {sloe}]
set_output_delay -add_delay -min -clock [get_clocks {ifclk}]  0.300 [get_ports {sloe}]
set_output_delay -add_delay -max -clock [get_clocks {ifclk}]  18.700 [get_ports {slrd}]
set_output_delay -add_delay -min -clock [get_clocks {ifclk}]  0.300 [get_ports {slrd}]
set_output_delay -add_delay -max -clock [get_clocks {ifclk}]  10.400 [get_ports {slwr}]
set_output_delay -add_delay -min -clock [get_clocks {ifclk}]  0.300 [get_ports {slwr}]


#**************************************************************
# Set Clock Groups
#**************************************************************

set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 
set_clock_groups -exclusive -group [get_clocks {ifclk_mux}] -group [get_clocks {double_period_scope_clk_mux}] 
set_clock_groups -exclusive -group [get_clocks {ifclk}] -group [get_clocks {scope_clk}] 
set_clock_groups -exclusive -group [get_clocks {double_period_scope_clk_mux}] -group [get_clocks {ifclk}] 
set_clock_groups -exclusive -group [get_clocks {ifclk}] -group [get_clocks {soc|scope_module_0|U1|U1|altpll_component|auto_generated|pll1|clk[0]}] 
set_clock_groups -exclusive -group [get_clocks {ifclk_mux}] -group [get_clocks {scope_clk}] 
set_clock_groups -exclusive -group [get_clocks {double_period_scope_clk}] -group [get_clocks {ifclk}] 


#**************************************************************
# Set False Path
#**************************************************************

set_false_path -to [get_keepers {*altera_std_synchronizer:*|din_s1}]
set_false_path -from [get_keepers {*rdptr_g*}] -to [get_keepers {*ws_dgrp|dffpipe_tu8:dffpipe12|dffe13a*}]
set_false_path -from [get_keepers {*delayed_wrptr_g*}] -to [get_keepers {*rs_dgwp|dffpipe_su8:dffpipe9|dffe10a*}]
set_false_path -from [get_keepers {*rdptr_g*}] -to [get_keepers {*ws_dgrp|dffpipe_ue9:dffpipe18|dffe19a*}]
set_false_path -from [get_keepers {*delayed_wrptr_g*}] -to [get_keepers {*rs_dgwp|dffpipe_se9:dffpipe14|dffe15a*}]
set_false_path -to [get_pins -nocase -compatibility_mode {*|alt_rst_sync_uq1|altera_reset_synchronizer_int_chain*|clrn}]
set_false_path -from [get_keepers {*nios2soc_nios2_gen2_0_cpu:*|nios2soc_nios2_gen2_0_cpu_nios2_oci:the_nios2soc_nios2_gen2_0_cpu_nios2_oci|nios2soc_nios2_gen2_0_cpu_nios2_oci_break:the_nios2soc_nios2_gen2_0_cpu_nios2_oci_break|break_readreg*}] -to [get_keepers {*nios2soc_nios2_gen2_0_cpu:*|nios2soc_nios2_gen2_0_cpu_nios2_oci:the_nios2soc_nios2_gen2_0_cpu_nios2_oci|nios2soc_nios2_gen2_0_cpu_debug_slave_wrapper:the_nios2soc_nios2_gen2_0_cpu_debug_slave_wrapper|nios2soc_nios2_gen2_0_cpu_debug_slave_tck:the_nios2soc_nios2_gen2_0_cpu_debug_slave_tck|*sr*}]
set_false_path -from [get_keepers {*nios2soc_nios2_gen2_0_cpu:*|nios2soc_nios2_gen2_0_cpu_nios2_oci:the_nios2soc_nios2_gen2_0_cpu_nios2_oci|nios2soc_nios2_gen2_0_cpu_nios2_oci_debug:the_nios2soc_nios2_gen2_0_cpu_nios2_oci_debug|*resetlatch}] -to [get_keepers {*nios2soc_nios2_gen2_0_cpu:*|nios2soc_nios2_gen2_0_cpu_nios2_oci:the_nios2soc_nios2_gen2_0_cpu_nios2_oci|nios2soc_nios2_gen2_0_cpu_debug_slave_wrapper:the_nios2soc_nios2_gen2_0_cpu_debug_slave_wrapper|nios2soc_nios2_gen2_0_cpu_debug_slave_tck:the_nios2soc_nios2_gen2_0_cpu_debug_slave_tck|*sr[33]}]
set_false_path -from [get_keepers {*nios2soc_nios2_gen2_0_cpu:*|nios2soc_nios2_gen2_0_cpu_nios2_oci:the_nios2soc_nios2_gen2_0_cpu_nios2_oci|nios2soc_nios2_gen2_0_cpu_nios2_oci_debug:the_nios2soc_nios2_gen2_0_cpu_nios2_oci_debug|monitor_ready}] -to [get_keepers {*nios2soc_nios2_gen2_0_cpu:*|nios2soc_nios2_gen2_0_cpu_nios2_oci:the_nios2soc_nios2_gen2_0_cpu_nios2_oci|nios2soc_nios2_gen2_0_cpu_debug_slave_wrapper:the_nios2soc_nios2_gen2_0_cpu_debug_slave_wrapper|nios2soc_nios2_gen2_0_cpu_debug_slave_tck:the_nios2soc_nios2_gen2_0_cpu_debug_slave_tck|*sr[0]}]
set_false_path -from [get_keepers {*nios2soc_nios2_gen2_0_cpu:*|nios2soc_nios2_gen2_0_cpu_nios2_oci:the_nios2soc_nios2_gen2_0_cpu_nios2_oci|nios2soc_nios2_gen2_0_cpu_nios2_oci_debug:the_nios2soc_nios2_gen2_0_cpu_nios2_oci_debug|monitor_error}] -to [get_keepers {*nios2soc_nios2_gen2_0_cpu:*|nios2soc_nios2_gen2_0_cpu_nios2_oci:the_nios2soc_nios2_gen2_0_cpu_nios2_oci|nios2soc_nios2_gen2_0_cpu_debug_slave_wrapper:the_nios2soc_nios2_gen2_0_cpu_debug_slave_wrapper|nios2soc_nios2_gen2_0_cpu_debug_slave_tck:the_nios2soc_nios2_gen2_0_cpu_debug_slave_tck|*sr[34]}]
set_false_path -from [get_keepers {*nios2soc_nios2_gen2_0_cpu:*|nios2soc_nios2_gen2_0_cpu_nios2_oci:the_nios2soc_nios2_gen2_0_cpu_nios2_oci|nios2soc_nios2_gen2_0_cpu_nios2_ocimem:the_nios2soc_nios2_gen2_0_cpu_nios2_ocimem|*MonDReg*}] -to [get_keepers {*nios2soc_nios2_gen2_0_cpu:*|nios2soc_nios2_gen2_0_cpu_nios2_oci:the_nios2soc_nios2_gen2_0_cpu_nios2_oci|nios2soc_nios2_gen2_0_cpu_debug_slave_wrapper:the_nios2soc_nios2_gen2_0_cpu_debug_slave_wrapper|nios2soc_nios2_gen2_0_cpu_debug_slave_tck:the_nios2soc_nios2_gen2_0_cpu_debug_slave_tck|*sr*}]
set_false_path -from [get_keepers {*nios2soc_nios2_gen2_0_cpu:*|nios2soc_nios2_gen2_0_cpu_nios2_oci:the_nios2soc_nios2_gen2_0_cpu_nios2_oci|nios2soc_nios2_gen2_0_cpu_debug_slave_wrapper:the_nios2soc_nios2_gen2_0_cpu_debug_slave_wrapper|nios2soc_nios2_gen2_0_cpu_debug_slave_tck:the_nios2soc_nios2_gen2_0_cpu_debug_slave_tck|*sr*}] -to [get_keepers {*nios2soc_nios2_gen2_0_cpu:*|nios2soc_nios2_gen2_0_cpu_nios2_oci:the_nios2soc_nios2_gen2_0_cpu_nios2_oci|nios2soc_nios2_gen2_0_cpu_debug_slave_wrapper:the_nios2soc_nios2_gen2_0_cpu_debug_slave_wrapper|nios2soc_nios2_gen2_0_cpu_debug_slave_sysclk:the_nios2soc_nios2_gen2_0_cpu_debug_slave_sysclk|*jdo*}]
set_false_path -from [get_keepers {sld_hub:*|irf_reg*}] -to [get_keepers {*nios2soc_nios2_gen2_0_cpu:*|nios2soc_nios2_gen2_0_cpu_nios2_oci:the_nios2soc_nios2_gen2_0_cpu_nios2_oci|nios2soc_nios2_gen2_0_cpu_debug_slave_wrapper:the_nios2soc_nios2_gen2_0_cpu_debug_slave_wrapper|nios2soc_nios2_gen2_0_cpu_debug_slave_sysclk:the_nios2soc_nios2_gen2_0_cpu_debug_slave_sysclk|ir*}]
set_false_path -from [get_keepers {sld_hub:*|sld_shadow_jsm:shadow_jsm|state[1]}] -to [get_keepers {*nios2soc_nios2_gen2_0_cpu:*|nios2soc_nios2_gen2_0_cpu_nios2_oci:the_nios2soc_nios2_gen2_0_cpu_nios2_oci|nios2soc_nios2_gen2_0_cpu_nios2_oci_debug:the_nios2soc_nios2_gen2_0_cpu_nios2_oci_debug|monitor_go}]


#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

