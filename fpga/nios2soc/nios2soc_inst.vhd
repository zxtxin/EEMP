	component nios2soc is
		port (
			clk_clk                                             : in    std_logic                     := 'X';             -- clk
			cy7c68013a_if_module_0_conduit_end_cha_fifo_data    : in    std_logic_vector(15 downto 0) := (others => 'X'); -- cha_fifo_data
			cy7c68013a_if_module_0_conduit_end_cha_fifo_rdempty : in    std_logic                     := 'X';             -- cha_fifo_rdempty
			cy7c68013a_if_module_0_conduit_end_cha_fifo_rdreq   : out   std_logic;                                        -- cha_fifo_rdreq
			cy7c68013a_if_module_0_conduit_end_chb_fifo_data    : in    std_logic_vector(15 downto 0) := (others => 'X'); -- chb_fifo_data
			cy7c68013a_if_module_0_conduit_end_chb_fifo_rdempty : in    std_logic                     := 'X';             -- chb_fifo_rdempty
			cy7c68013a_if_module_0_conduit_end_chb_fifo_rdreq   : out   std_logic;                                        -- chb_fifo_rdreq
			cy7c68013a_if_module_0_conduit_end_la_fifo_data     : in    std_logic_vector(15 downto 0) := (others => 'X'); -- la_fifo_data
			cy7c68013a_if_module_0_conduit_end_la_fifo_rdempty  : in    std_logic                     := 'X';             -- la_fifo_rdempty
			cy7c68013a_if_module_0_conduit_end_la_fifo_rdreq    : out   std_logic;                                        -- la_fifo_rdreq
			cy7c68013a_if_module_0_conduit_end_flaga_raw        : in    std_logic                     := 'X';             -- flaga_raw
			cy7c68013a_if_module_0_conduit_end_flagc_raw        : in    std_logic                     := 'X';             -- flagc_raw
			cy7c68013a_if_module_0_conduit_end_flagb_raw        : in    std_logic                     := 'X';             -- flagb_raw
			cy7c68013a_if_module_0_conduit_end_slwr             : out   std_logic;                                        -- slwr
			cy7c68013a_if_module_0_conduit_end_slrd             : out   std_logic;                                        -- slrd
			cy7c68013a_if_module_0_conduit_end_sloe             : out   std_logic;                                        -- sloe
			cy7c68013a_if_module_0_conduit_end_pktend           : out   std_logic;                                        -- pktend
			cy7c68013a_if_module_0_conduit_end_fifoadr          : out   std_logic_vector(1 downto 0);                     -- fifoadr
			cy7c68013a_if_module_0_conduit_end_fd               : inout std_logic_vector(15 downto 0) := (others => 'X'); -- fd
			cy7c68013a_if_module_0_conduit_end_soc_ready        : out   std_logic;                                        -- soc_ready
			pio_0_external_connection_export                    : inout std_logic_vector(31 downto 0) := (others => 'X'); -- export
			reset_reset_n                                       : in    std_logic                     := 'X';             -- reset_n
			scope_module_0_conduit_end_chb_fifo_data            : out   std_logic_vector(15 downto 0);                    -- chb_fifo_data
			scope_module_0_conduit_end_chb_fifo_rdempty         : out   std_logic;                                        -- chb_fifo_rdempty
			scope_module_0_conduit_end_chb_fifo_rdreq           : in    std_logic                     := 'X';             -- chb_fifo_rdreq
			scope_module_0_conduit_end_cha_fifo_data            : out   std_logic_vector(15 downto 0);                    -- cha_fifo_data
			scope_module_0_conduit_end_cha_fifo_rdempty         : out   std_logic;                                        -- cha_fifo_rdempty
			scope_module_0_conduit_end_cha_fifo_rdreq           : in    std_logic                     := 'X';             -- cha_fifo_rdreq
			scope_module_0_conduit_end_adc_clk                  : out   std_logic;                                        -- adc_clk
			scope_module_0_conduit_end_chb_data                 : in    std_logic_vector(7 downto 0)  := (others => 'X'); -- chb_data
			scope_module_0_conduit_end_cha_data                 : in    std_logic_vector(7 downto 0)  := (others => 'X'); -- cha_data
			spi_0_external_MISO                                 : in    std_logic                     := 'X';             -- MISO
			spi_0_external_MOSI                                 : out   std_logic;                                        -- MOSI
			spi_0_external_SCLK                                 : out   std_logic;                                        -- SCLK
			spi_0_external_SS_n                                 : out   std_logic_vector(2 downto 0)                      -- SS_n
		);
	end component nios2soc;

	u0 : component nios2soc
		port map (
			clk_clk                                             => CONNECTED_TO_clk_clk,                                             --                                clk.clk
			cy7c68013a_if_module_0_conduit_end_cha_fifo_data    => CONNECTED_TO_cy7c68013a_if_module_0_conduit_end_cha_fifo_data,    -- cy7c68013a_if_module_0_conduit_end.cha_fifo_data
			cy7c68013a_if_module_0_conduit_end_cha_fifo_rdempty => CONNECTED_TO_cy7c68013a_if_module_0_conduit_end_cha_fifo_rdempty, --                                   .cha_fifo_rdempty
			cy7c68013a_if_module_0_conduit_end_cha_fifo_rdreq   => CONNECTED_TO_cy7c68013a_if_module_0_conduit_end_cha_fifo_rdreq,   --                                   .cha_fifo_rdreq
			cy7c68013a_if_module_0_conduit_end_chb_fifo_data    => CONNECTED_TO_cy7c68013a_if_module_0_conduit_end_chb_fifo_data,    --                                   .chb_fifo_data
			cy7c68013a_if_module_0_conduit_end_chb_fifo_rdempty => CONNECTED_TO_cy7c68013a_if_module_0_conduit_end_chb_fifo_rdempty, --                                   .chb_fifo_rdempty
			cy7c68013a_if_module_0_conduit_end_chb_fifo_rdreq   => CONNECTED_TO_cy7c68013a_if_module_0_conduit_end_chb_fifo_rdreq,   --                                   .chb_fifo_rdreq
			cy7c68013a_if_module_0_conduit_end_la_fifo_data     => CONNECTED_TO_cy7c68013a_if_module_0_conduit_end_la_fifo_data,     --                                   .la_fifo_data
			cy7c68013a_if_module_0_conduit_end_la_fifo_rdempty  => CONNECTED_TO_cy7c68013a_if_module_0_conduit_end_la_fifo_rdempty,  --                                   .la_fifo_rdempty
			cy7c68013a_if_module_0_conduit_end_la_fifo_rdreq    => CONNECTED_TO_cy7c68013a_if_module_0_conduit_end_la_fifo_rdreq,    --                                   .la_fifo_rdreq
			cy7c68013a_if_module_0_conduit_end_flaga_raw        => CONNECTED_TO_cy7c68013a_if_module_0_conduit_end_flaga_raw,        --                                   .flaga_raw
			cy7c68013a_if_module_0_conduit_end_flagc_raw        => CONNECTED_TO_cy7c68013a_if_module_0_conduit_end_flagc_raw,        --                                   .flagc_raw
			cy7c68013a_if_module_0_conduit_end_flagb_raw        => CONNECTED_TO_cy7c68013a_if_module_0_conduit_end_flagb_raw,        --                                   .flagb_raw
			cy7c68013a_if_module_0_conduit_end_slwr             => CONNECTED_TO_cy7c68013a_if_module_0_conduit_end_slwr,             --                                   .slwr
			cy7c68013a_if_module_0_conduit_end_slrd             => CONNECTED_TO_cy7c68013a_if_module_0_conduit_end_slrd,             --                                   .slrd
			cy7c68013a_if_module_0_conduit_end_sloe             => CONNECTED_TO_cy7c68013a_if_module_0_conduit_end_sloe,             --                                   .sloe
			cy7c68013a_if_module_0_conduit_end_pktend           => CONNECTED_TO_cy7c68013a_if_module_0_conduit_end_pktend,           --                                   .pktend
			cy7c68013a_if_module_0_conduit_end_fifoadr          => CONNECTED_TO_cy7c68013a_if_module_0_conduit_end_fifoadr,          --                                   .fifoadr
			cy7c68013a_if_module_0_conduit_end_fd               => CONNECTED_TO_cy7c68013a_if_module_0_conduit_end_fd,               --                                   .fd
			cy7c68013a_if_module_0_conduit_end_soc_ready        => CONNECTED_TO_cy7c68013a_if_module_0_conduit_end_soc_ready,        --                                   .soc_ready
			pio_0_external_connection_export                    => CONNECTED_TO_pio_0_external_connection_export,                    --          pio_0_external_connection.export
			reset_reset_n                                       => CONNECTED_TO_reset_reset_n,                                       --                              reset.reset_n
			scope_module_0_conduit_end_chb_fifo_data            => CONNECTED_TO_scope_module_0_conduit_end_chb_fifo_data,            --         scope_module_0_conduit_end.chb_fifo_data
			scope_module_0_conduit_end_chb_fifo_rdempty         => CONNECTED_TO_scope_module_0_conduit_end_chb_fifo_rdempty,         --                                   .chb_fifo_rdempty
			scope_module_0_conduit_end_chb_fifo_rdreq           => CONNECTED_TO_scope_module_0_conduit_end_chb_fifo_rdreq,           --                                   .chb_fifo_rdreq
			scope_module_0_conduit_end_cha_fifo_data            => CONNECTED_TO_scope_module_0_conduit_end_cha_fifo_data,            --                                   .cha_fifo_data
			scope_module_0_conduit_end_cha_fifo_rdempty         => CONNECTED_TO_scope_module_0_conduit_end_cha_fifo_rdempty,         --                                   .cha_fifo_rdempty
			scope_module_0_conduit_end_cha_fifo_rdreq           => CONNECTED_TO_scope_module_0_conduit_end_cha_fifo_rdreq,           --                                   .cha_fifo_rdreq
			scope_module_0_conduit_end_adc_clk                  => CONNECTED_TO_scope_module_0_conduit_end_adc_clk,                  --                                   .adc_clk
			scope_module_0_conduit_end_chb_data                 => CONNECTED_TO_scope_module_0_conduit_end_chb_data,                 --                                   .chb_data
			scope_module_0_conduit_end_cha_data                 => CONNECTED_TO_scope_module_0_conduit_end_cha_data,                 --                                   .cha_data
			spi_0_external_MISO                                 => CONNECTED_TO_spi_0_external_MISO,                                 --                     spi_0_external.MISO
			spi_0_external_MOSI                                 => CONNECTED_TO_spi_0_external_MOSI,                                 --                                   .MOSI
			spi_0_external_SCLK                                 => CONNECTED_TO_spi_0_external_SCLK,                                 --                                   .SCLK
			spi_0_external_SS_n                                 => CONNECTED_TO_spi_0_external_SS_n                                  --                                   .SS_n
		);

