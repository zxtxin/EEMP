	nios2soc u0 (
		.clk_clk                                             (<connected-to-clk_clk>),                                             //                                clk.clk
		.cy7c68013a_if_module_0_conduit_end_cha_fifo_data    (<connected-to-cy7c68013a_if_module_0_conduit_end_cha_fifo_data>),    // cy7c68013a_if_module_0_conduit_end.cha_fifo_data
		.cy7c68013a_if_module_0_conduit_end_cha_fifo_rdempty (<connected-to-cy7c68013a_if_module_0_conduit_end_cha_fifo_rdempty>), //                                   .cha_fifo_rdempty
		.cy7c68013a_if_module_0_conduit_end_cha_fifo_rdreq   (<connected-to-cy7c68013a_if_module_0_conduit_end_cha_fifo_rdreq>),   //                                   .cha_fifo_rdreq
		.cy7c68013a_if_module_0_conduit_end_chb_fifo_data    (<connected-to-cy7c68013a_if_module_0_conduit_end_chb_fifo_data>),    //                                   .chb_fifo_data
		.cy7c68013a_if_module_0_conduit_end_chb_fifo_rdempty (<connected-to-cy7c68013a_if_module_0_conduit_end_chb_fifo_rdempty>), //                                   .chb_fifo_rdempty
		.cy7c68013a_if_module_0_conduit_end_chb_fifo_rdreq   (<connected-to-cy7c68013a_if_module_0_conduit_end_chb_fifo_rdreq>),   //                                   .chb_fifo_rdreq
		.cy7c68013a_if_module_0_conduit_end_la_fifo_data     (<connected-to-cy7c68013a_if_module_0_conduit_end_la_fifo_data>),     //                                   .la_fifo_data
		.cy7c68013a_if_module_0_conduit_end_la_fifo_rdempty  (<connected-to-cy7c68013a_if_module_0_conduit_end_la_fifo_rdempty>),  //                                   .la_fifo_rdempty
		.cy7c68013a_if_module_0_conduit_end_la_fifo_rdreq    (<connected-to-cy7c68013a_if_module_0_conduit_end_la_fifo_rdreq>),    //                                   .la_fifo_rdreq
		.cy7c68013a_if_module_0_conduit_end_flaga_raw        (<connected-to-cy7c68013a_if_module_0_conduit_end_flaga_raw>),        //                                   .flaga_raw
		.cy7c68013a_if_module_0_conduit_end_flagc_raw        (<connected-to-cy7c68013a_if_module_0_conduit_end_flagc_raw>),        //                                   .flagc_raw
		.cy7c68013a_if_module_0_conduit_end_flagb_raw        (<connected-to-cy7c68013a_if_module_0_conduit_end_flagb_raw>),        //                                   .flagb_raw
		.cy7c68013a_if_module_0_conduit_end_slwr             (<connected-to-cy7c68013a_if_module_0_conduit_end_slwr>),             //                                   .slwr
		.cy7c68013a_if_module_0_conduit_end_slrd             (<connected-to-cy7c68013a_if_module_0_conduit_end_slrd>),             //                                   .slrd
		.cy7c68013a_if_module_0_conduit_end_sloe             (<connected-to-cy7c68013a_if_module_0_conduit_end_sloe>),             //                                   .sloe
		.cy7c68013a_if_module_0_conduit_end_pktend           (<connected-to-cy7c68013a_if_module_0_conduit_end_pktend>),           //                                   .pktend
		.cy7c68013a_if_module_0_conduit_end_fifoadr          (<connected-to-cy7c68013a_if_module_0_conduit_end_fifoadr>),          //                                   .fifoadr
		.cy7c68013a_if_module_0_conduit_end_fd               (<connected-to-cy7c68013a_if_module_0_conduit_end_fd>),               //                                   .fd
		.cy7c68013a_if_module_0_conduit_end_soc_ready        (<connected-to-cy7c68013a_if_module_0_conduit_end_soc_ready>),        //                                   .soc_ready
		.pio_0_external_connection_export                    (<connected-to-pio_0_external_connection_export>),                    //          pio_0_external_connection.export
		.reset_reset_n                                       (<connected-to-reset_reset_n>),                                       //                              reset.reset_n
		.scope_module_0_conduit_end_chb_fifo_data            (<connected-to-scope_module_0_conduit_end_chb_fifo_data>),            //         scope_module_0_conduit_end.chb_fifo_data
		.scope_module_0_conduit_end_chb_fifo_rdempty         (<connected-to-scope_module_0_conduit_end_chb_fifo_rdempty>),         //                                   .chb_fifo_rdempty
		.scope_module_0_conduit_end_chb_fifo_rdreq           (<connected-to-scope_module_0_conduit_end_chb_fifo_rdreq>),           //                                   .chb_fifo_rdreq
		.scope_module_0_conduit_end_cha_fifo_data            (<connected-to-scope_module_0_conduit_end_cha_fifo_data>),            //                                   .cha_fifo_data
		.scope_module_0_conduit_end_cha_fifo_rdempty         (<connected-to-scope_module_0_conduit_end_cha_fifo_rdempty>),         //                                   .cha_fifo_rdempty
		.scope_module_0_conduit_end_cha_fifo_rdreq           (<connected-to-scope_module_0_conduit_end_cha_fifo_rdreq>),           //                                   .cha_fifo_rdreq
		.scope_module_0_conduit_end_adc_clk                  (<connected-to-scope_module_0_conduit_end_adc_clk>),                  //                                   .adc_clk
		.scope_module_0_conduit_end_chb_data                 (<connected-to-scope_module_0_conduit_end_chb_data>),                 //                                   .chb_data
		.scope_module_0_conduit_end_cha_data                 (<connected-to-scope_module_0_conduit_end_cha_data>),                 //                                   .cha_data
		.spi_0_external_MISO                                 (<connected-to-spi_0_external_MISO>),                                 //                     spi_0_external.MISO
		.spi_0_external_MOSI                                 (<connected-to-spi_0_external_MOSI>),                                 //                                   .MOSI
		.spi_0_external_SCLK                                 (<connected-to-spi_0_external_SCLK>),                                 //                                   .SCLK
		.spi_0_external_SS_n                                 (<connected-to-spi_0_external_SS_n>)                                  //                                   .SS_n
	);

