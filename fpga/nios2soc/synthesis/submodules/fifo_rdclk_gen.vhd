library ieee;
use ieee.std_logic_1164.all;
entity fifo_rdclk_gen is
	port(	scope_clk: in std_logic;
			ifclk: in std_logic;
			rdclk: out std_logic;
			rdclk_sel: in std_logic
	);
end fifo_rdclk_gen;
architecture rtl of fifo_rdclk_gen is
COMPONENT unrelated_clk_switch
	PORT
	(
		out_clk		:	 OUT STD_LOGIC;
		clk_0		:	 IN STD_LOGIC;
		clk_1		:	 IN STD_LOGIC;
		sel		:	 IN STD_LOGIC  
	);
END COMPONENT;
signal double_period_scope_clk:std_logic;
begin
	process(scope_clk)
	begin
		if(scope_clk = '1' and scope_clk'event) then
			double_period_scope_clk <= not double_period_scope_clk;
		end if;
	end process;
	U1: unrelated_clk_switch port map (
        out_clk => rdclk,
        clk_0 => ifclk,
        clk_1 => double_period_scope_clk,
        sel => rdclk_sel
        );
end rtl;