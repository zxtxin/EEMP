library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
entity force_trigger_counter is
	generic (count : natural := 5);
	port (rst: in std_logic;
            start: in std_logic;
			clk: in std_logic;
			trigger_enable: in std_logic;
			force_trigger_pulse: out std_logic
			);
end force_trigger_counter;
architecture rtl of force_trigger_counter is
type states is (idle,counting);
signal pr_state,nx_state: states;
signal counter : natural range 0 to count;
signal reset_condition:std_logic;
begin
    reset_condition <= '1' when (counter = count) or (trigger_enable = '0') else '0';
    process(pr_state,start,reset_condition)
    begin
        case pr_state is
            when idle =>
                if(start = '1') then
                    nx_state <= counting;
                else    
                    nx_state <= pr_state;
                end if;
            when counting =>
                if(reset_condition = '1' ) then
                    nx_state <= idle;
                else
                    nx_state <= pr_state;
                end if;
        end case;
    end process;
	process(rst,clk)
	begin
		if(rst = '1') then
			counter <= 0;
            pr_state <= idle;
		elsif(clk'event and clk = '1') then
            pr_state <= nx_state;
            if(reset_condition = '1') then
                counter <= 0;
            elsif(pr_state = counting) then
                counter <= counter + 1;
            end if;
		end if;	
	end process;
	force_trigger_pulse <= '1' when counter = count else '0';
end rtl;