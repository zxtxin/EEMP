library IEEE;
use IEEE.std_logic_1164.all;
entity if_rx is
	port(
		data_rx: in std_logic_vector(15 downto 0);
		start_rx: in std_logic;
		received_packet: out std_logic_vector(31 downto 0);
		slrd: out std_logic;
		sloe: out std_logic;
		rx_busy: out std_logic;
		ifclk: in std_logic;
		rst: in std_logic
	);
end if_rx;
architecture arch_if_rx of if_rx is
type states is (idle,first_word,second_word);
signal pr_state,nx_state:states;
begin
	process(ifclk,rst)
	begin
		if(rst = '1') then
			pr_state <= idle;
		elsif(ifclk = '1' and ifclk'event) then
			pr_state <= nx_state;				
		end if;
	end process;
	process(pr_state,start_rx)
	begin
		case pr_state is
			when idle =>
				if(start_rx = '1') then
					nx_state <= first_word;
				else
					nx_state <= pr_state;
				end if;
			when first_word =>
				nx_state <= second_word;
			when second_word =>
				nx_state <= idle;
		end case;
	end process;

	process(ifclk,rst)
	begin
		if(rst = '1') then
			received_packet <= (others => '0');
			sloe <= '1';
			rx_busy <= '0';
		elsif(ifclk = '1' and ifclk'event) then
			case nx_state is 
				when first_word =>
					slrd <= '0';
					sloe <= '0';
					rx_busy <= '1';
				when idle =>
					slrd <= '1';
					sloe <= '1';
					rx_busy <= '0';
				when others =>
					null;
			end case;
			case pr_state is
				when first_word =>
					received_packet(15 downto 0) <= data_rx;
				when second_word =>
					received_packet(31 downto 16) <= data_rx;
				when others =>
					null;
			end case;
		end if;
	end process;
end arch_if_rx;