library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity pretrigger_gen is
	port ( 	trigger_position: in std_logic_vector(12 downto 0);
			counter_en:	in std_logic;
			pretrigger_pulse: out std_logic;
			sysclk: in std_logic;
			rst: in std_logic	
	);
end pretrigger_gen;
architecture rtl of pretrigger_gen is
signal counter: std_logic_vector(12 downto 0):= (others => '0');
begin
	process(sysclk,rst)
	begin
		if(rst = '1' ) then
			counter <= (others => '0');
		elsif(sysclk = '1' and sysclk'event ) then
			if(counter_en = '1') then
				if(counter = trigger_position) then
					counter <= (others => '0');
				else
					counter <= counter + 1;
				end if;
			end if;
		end if;
	end process;
	pretrigger_pulse <= '1' when counter = trigger_position else '0';
end rtl;