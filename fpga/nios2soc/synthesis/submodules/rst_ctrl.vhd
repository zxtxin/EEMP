library ieee;
use ieee.std_logic_1164.all;
entity rst_ctrl is
	port (
		rst_in: in std_logic;
		clk: in std_logic;
		rst_out: out std_logic;
		nrst_out: out std_logic
	);
end rst_ctrl;
architecture arch_rst_ctrl of rst_ctrl is
signal ff1,ff2: std_logic;
begin
	process(clk,rst_in)
	begin
		if(rst_in = '1') then
			ff1 <= '0';
			ff2 <= '0';
		elsif(clk'event and clk = '1') then
			ff1 <= '1';
			ff2 <= ff1;
		end if;
	end process;
	nrst_out <= ff2;
	rst_out <= not ff2;
end arch_rst_ctrl;