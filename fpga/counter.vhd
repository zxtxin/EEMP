library ieee;
use ieee.std_logic_1164.all;
entity counter is
	generic (max : natural := 511);
	port ( enable: in std_logic;
		   current_val: out natural;
		   expired: out std_logic;
		   clk:in std_logic;
		   rst: in std_logic
	);
end counter;
architecture arch_counter of counter is
signal counter: natural range 0 to max;
begin
	process(clk,rst)
	begin
		if(rst = '1') then
			counter <= 0;
		elsif(clk'event and clk = '1') then
			if(counter = max) then
				counter <= 0;
			elsif(enable = '1') then
				counter <= counter + 1;
			end if;
		end if;
	end process;
	current_val <= counter;
	expired <= '1' when counter = max else '0';
end arch_counter;
