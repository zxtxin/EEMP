#ifndef REQ_CODE_H_
#define REQ_CODE_H_

#define POWER_ON_OFF				0x11	
#define GET_EPSTAT 					0x12
#define GET_POWER_STATUS			0x13
#define FPGA_GLOBAL_RST				0x14
#define RST_EP2_BUF					0x15
#define RST_EP8_BUF					0x16
#define GET_SOC_STAT				0x17
#define FPGA_GLOBAL_RST_ACTIVE		0x18
#define FPGA_GLOBAL_RST_INACTIVE	0x19
#endif