//-----------------------------------------------------------------------------
//   File:      periph.c
//   Contents:  Hooks required to implement USB peripheral function.
//
// $Archive: /USB/Target/Fw/lp/periph.c $
// $Date: 3/23/05 3:03p $
// $Revision: 3 $
//
//
//-----------------------------------------------------------------------------
// Copyright 2003, Cypress Semiconductor Corporation
//
// This software is owned by Cypress Semiconductor Corporation (Cypress) and is
// protected by United States copyright laws and international treaty provisions. Cypress
// hereby grants to Licensee a personal, non-exclusive, non-transferable license to copy,
// use, modify, create derivative works of, and compile the Cypress Source Code and
// derivative works for the sole purpose of creating custom software in support of Licensee
// product ("Licensee Product") to be used only in conjunction with a Cypress integrated
// circuit. Any reproduction, modification, translation, compilation, or representation of this
// software except as specified above is prohibited without the express written permission of
// Cypress.
//
// Disclaimer: Cypress makes no warranty of any kind, express or implied, with regard to
// this material, including, but not limited to, the implied warranties of merchantability and
// fitness for a particular purpose. Cypress reserves the right to make changes without
// further notice to the materials described herein. Cypress does not assume any liability
// arising out of the application or use of any product or circuit described herein. Cypress�
// products described herein are not authorized for use as components in life-support
// devices.
//
// This software is protected by and subject to worldwide patent coverage, including U.S.
// and foreign patents. Use may be limited by and subject to the Cypress Software License
// Agreement.
//-----------------------------------------------------------------------------
#pragma NOIV               // Do not generate interrupt vectors

#include "fx2.h"
#include "fx2regs.h"
#include "syncdly.h"            // SYNCDELAY macro
#include "req_code.h"
extern BOOL   GotSUD;         // Received setup data flag
extern BOOL   Sleep;
extern BOOL   Rwuen;
extern BOOL   Selfpwr;

BYTE   Configuration;      // Current configuration
BYTE   AlternateSetting;   // Alternate settings


sbit EP0BSY = 0xBA +0;
sbit EP1OUTBSY = 0xBA + 1;
sbit EP1INBSY = 0xBA + 2;



#define EP0BUFF_SIZE	0x40
//-----------------------------------------------------------------------------
// Task Dispatcher hooks
//   The following hooks are called by the task dispatcher.
//-----------------------------------------------------------------------------

void TD_Init(void)             // Called once at startup
{
//   BREAKPT &= ~bmBPEN;      // to see BKPT LED go out TGE

	
	
   	CPUCS = 0x12; // CLKSPD[1:0]=10, for 48MHz operation, output CLKOUT
   	SYNCDELAY;

   	REVCTL = 0x03;
   	SYNCDELAY;
   	IFCONFIG = 0xa3; //Internal clock, 30 MHz, Slave FIFO interface
   	SYNCDELAY;
   	EP4CFG &= 0x7f;                // invalid
   	SYNCDELAY;  
   	EP6CFG &= 0x7f;                // invalid
   	SYNCDELAY;  
   	EP2CFG = 0xEB;                // in 1024 bytes, 3x, bulk
   	SYNCDELAY;                                         
   	EP8CFG = 0xA2;                // out 512 bytes, 2x, bulk
   	SYNCDELAY; 
   	FIFORESET = 0x80;             // activate NAK-ALL to avoid race conditions
   	SYNCDELAY; 
   	FIFORESET = 0x82;             // reset, FIFO2
   	SYNCDELAY; 
   	FIFORESET = 0x84;             // reset, FIFO4
   	SYNCDELAY; 
   	FIFORESET = 0x86;             // reset, FIFO6
   	SYNCDELAY; 
   	FIFORESET = 0x88;             // reset, FIFO8
   	SYNCDELAY; 
   	FIFORESET = 0x00;             // deactivate NAK-ALL
   	SYNCDELAY;
   	OUTPKTEND = 0x88; // Arm both EP8 buffers to ��prime the pump��
   	SYNCDELAY;
   	OUTPKTEND = 0x88;
   	SYNCDELAY;
   	EP2FIFOCFG = 0x0D;            // AUTOIN=1, ZEROLENIN=1, WORDWIDE=1
   	SYNCDELAY;
   	EP8FIFOCFG = 0x11;            // AUTOOUT=1, WORDWIDE=1
   	SYNCDELAY;
   	PINFLAGSAB = 0xCB;
   	SYNCDELAY;
   	FIFOPINPOLAR = 0x00;			// active low
   	SYNCDELAY;
   	EP2AUTOINLENH = 0x04; // Auto-commit 1024-byte packets
   	SYNCDELAY;
  	EP2AUTOINLENL = 0x00;
   	SYNCDELAY;
   
	EP0BCH = 0;

	OEA = 0x81;
	PA0 = 0;
	PA7 = 0;
	


	// default to I2C bus freq = 100 KHz
	I2CTL &= ~bm400KHZ;	// 0: 100 KHz
}

void TD_Poll(void)             // Called repeatedly while the device is idle
{
}

BOOL TD_Suspend(void)          // Called before the device goes into suspend mode
{
   return(TRUE);
}

BOOL TD_Resume(void)          // Called after the device resumes
{
   return(TRUE);
}

//-----------------------------------------------------------------------------
// Device Request hooks
//   The following hooks are called by the end point 0 device request parser.
//-----------------------------------------------------------------------------

BOOL DR_GetDescriptor(void)
{
   return(TRUE);
}

BOOL DR_SetConfiguration(void)   // Called when a Set Configuration command is received
{
   Configuration = SETUPDAT[2];
   return(TRUE);            // Handled by user code
}

BOOL DR_GetConfiguration(void)   // Called when a Get Configuration command is received
{
   EP0BUF[0] = Configuration;
   EP0BCH = 0;
   EP0BCL = 1;
   return(TRUE);            // Handled by user code
}

BOOL DR_SetInterface(void)       // Called when a Set Interface command is received
{
   AlternateSetting = SETUPDAT[2];
   return(TRUE);            // Handled by user code
}

BOOL DR_GetInterface(void)       // Called when a Set Interface command is received
{
   EP0BUF[0] = AlternateSetting;
   EP0BCH = 0;
   EP0BCL = 1;
   return(TRUE);            // Handled by user code
}

BOOL DR_GetStatus(void)
{
   return(TRUE);
}

BOOL DR_ClearFeature(void)
{
   return(TRUE);
}

BOOL DR_SetFeature(void)
{
   return(TRUE);
}

BOOL DR_VendorCmnd(void)
{
   	switch(SETUPDAT[1])
	{	
		case POWER_ON_OFF:
			PA7 = !PA7;
		break;
		case GET_EPSTAT:
			*EP0BUF = EP2468STAT;
			EP0BCL = 1;
		break;
		case GET_POWER_STATUS:
			*EP0BUF = PA7;
			EP0BCL = 1;
		break;
		case FPGA_GLOBAL_RST:
			PA0 = 1;
			PA0 = 0;
		break;
		case FPGA_GLOBAL_RST_ACTIVE:
			PA0 = 1;
		break;
		case FPGA_GLOBAL_RST_INACTIVE:
			PA0 = 0;
		break;
		case RST_EP2_BUF:
			FIFORESET = 0x80; // activate NAK-ALL to avoid race conditions
			SYNCDELAY;
			EP2FIFOCFG = 0x00;//switching to manual mode
			SYNCDELAY;
			FIFORESET = 0x02; // Reset FIFO 2
			SYNCDELAY;
			FIFORESET = 0x00; //Release NAKALL
			SYNCDELAY;
			EP2FIFOCFG = 0x0D;// AUTOIN=1, ZEROLENIN=1, WORDWIDE=1
   			SYNCDELAY;
//			EP2AUTOINLENH = 0x04; // Auto-commit 1024-byte packets
   			SYNCDELAY;
//			EP2AUTOINLENL = 0x00;
   			SYNCDELAY;
		break;
		case RST_EP8_BUF:
			FIFORESET = 0x80; // activate NAK-ALL to avoid race conditions
			SYNCDELAY;
			EP8FIFOCFG = 0x00;//switching to manual mode
			SYNCDELAY;
			FIFORESET = 0x08; // Reset FIFO 8
			SYNCDELAY;
			FIFORESET = 0x00; //Release NAKALL
			SYNCDELAY;
			OUTPKTEND = 0x88; // Arm both EP8 buffers to ��prime the pump��
   			SYNCDELAY;
   			OUTPKTEND = 0x88;
   			SYNCDELAY;
			EP8FIFOCFG = 0x11;// AUTOOUT=1, WORDWIDE=1
			SYNCDELAY;
		break;
		case GET_SOC_STAT:
			*EP0BUF = PA1;
			EP0BCL = 1;
		break;
	}
	return(FALSE); // no error; command handled OK
}

//-----------------------------------------------------------------------------
// USB Interrupt Handlers
//   The following functions are called by the USB interrupt jump table.
//-----------------------------------------------------------------------------

// Setup Data Available Interrupt Handler
void ISR_Sudav(void) interrupt 0
{
   GotSUD = TRUE;            // Set flag
   EZUSB_IRQ_CLEAR();
   USBIRQ = bmSUDAV;         // Clear SUDAV IRQ
}

// Setup Token Interrupt Handler
void ISR_Sutok(void) interrupt 0
{
   EZUSB_IRQ_CLEAR();
   USBIRQ = bmSUTOK;         // Clear SUTOK IRQ
}

void ISR_Sof(void) interrupt 0
{
   EZUSB_IRQ_CLEAR();
   USBIRQ = bmSOF;            // Clear SOF IRQ
}

void ISR_Ures(void) interrupt 0
{
   // whenever we get a USB reset, we should revert to full speed mode
   pConfigDscr = pFullSpeedConfigDscr;
   ((CONFIGDSCR xdata *) pConfigDscr)->type = CONFIG_DSCR;
   pOtherConfigDscr = pHighSpeedConfigDscr;
   ((CONFIGDSCR xdata *) pOtherConfigDscr)->type = OTHERSPEED_DSCR;
   
   EZUSB_IRQ_CLEAR();
   USBIRQ = bmURES;         // Clear URES IRQ
}

void ISR_Susp(void) interrupt 0
{
   Sleep = TRUE;
   EZUSB_IRQ_CLEAR();
   USBIRQ = bmSUSP;
}

void ISR_Highspeed(void) interrupt 0
{
   if (EZUSB_HIGHSPEED())
   {
      pConfigDscr = pHighSpeedConfigDscr;
      ((CONFIGDSCR xdata *) pConfigDscr)->type = CONFIG_DSCR;
      pOtherConfigDscr = pFullSpeedConfigDscr;
      ((CONFIGDSCR xdata *) pOtherConfigDscr)->type = OTHERSPEED_DSCR;
   }

   EZUSB_IRQ_CLEAR();
   USBIRQ = bmHSGRANT;
}
void ISR_Ep0ack(void) interrupt 0
{
}
void ISR_Stub(void) interrupt 0
{
}
void ISR_Ep0in(void) interrupt 0
{
}
void ISR_Ep0out(void) interrupt 0
{
}
void ISR_Ep1in(void) interrupt 0
{
}
void ISR_Ep1out(void) interrupt 0
{
}
void ISR_Ep2inout(void) interrupt 0
{
}
void ISR_Ep4inout(void) interrupt 0
{
}
void ISR_Ep6inout(void) interrupt 0
{
}
void ISR_Ep8inout(void) interrupt 0
{
}
void ISR_Ibn(void) interrupt 0
{
}
void ISR_Ep0pingnak(void) interrupt 0
{
}
void ISR_Ep1pingnak(void) interrupt 0
{
}
void ISR_Ep2pingnak(void) interrupt 0
{
}
void ISR_Ep4pingnak(void) interrupt 0
{
}
void ISR_Ep6pingnak(void) interrupt 0
{
}
void ISR_Ep8pingnak(void) interrupt 0
{
}
void ISR_Errorlimit(void) interrupt 0
{
}
void ISR_Ep2piderror(void) interrupt 0
{
}
void ISR_Ep4piderror(void) interrupt 0
{
}
void ISR_Ep6piderror(void) interrupt 0
{
}
void ISR_Ep8piderror(void) interrupt 0
{
}
void ISR_Ep2pflag(void) interrupt 0
{
}
void ISR_Ep4pflag(void) interrupt 0
{
}
void ISR_Ep6pflag(void) interrupt 0
{
}
void ISR_Ep8pflag(void) interrupt 0
{
}
void ISR_Ep2eflag(void) interrupt 0
{
}
void ISR_Ep4eflag(void) interrupt 0
{
}
void ISR_Ep6eflag(void) interrupt 0
{
}
void ISR_Ep8eflag(void) interrupt 0
{
}
void ISR_Ep2fflag(void) interrupt 0
{
}
void ISR_Ep4fflag(void) interrupt 0
{
}
void ISR_Ep6fflag(void) interrupt 0
{
}
void ISR_Ep8fflag(void) interrupt 0
{
}
void ISR_GpifComplete(void) interrupt 0
{
}
void ISR_GpifWaveform(void) interrupt 0
{
}
