﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="13008000">
	<Property Name="NI.Lib.Icon" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!./!!!*Q(C=\:;R4FN*&amp;)90:+6M&amp;`%%7;(N9+PT"B'VK7SK&gt;4E0E!:4!;*R(O%I.UI"S5J%.&amp;O9QN&gt;9QN:;*'&gt;DC"1[R"O10MXEG`&amp;!UE#1&gt;F@;)H0Z@;``-_?=&lt;_:?LCS3R]]C0UH]XJ#\RB,R8W]@3R^3P(@LK/Y4@R&lt;0[\/E?H#;TCFQ&gt;DS?^M:VF@C7[X%^1//4]@'A0DR/]6&amp;P50='I_ID]&gt;`C3?])D5`?(2_6`('-^7(^.M?8[R(ZIXLU\CXZM]9J`SD'PT\G`H&amp;SE04_V@4M\SK.&amp;Q?4V[@HV?NJP(P^D&lt;S_RKWDOE_=^7^E6;UX"`']A\`[&lt;,@^PN'O:OP@W/[U5(P3XGVV6D&gt;4YO?HL5[DO6H70WGMI&gt;`X&gt;H&lt;8:PG.&gt;IT6;G&gt;\NP[.L9U7_PT(S7;L-WO=]N?_LP^&amp;VN\W^'#3Q[H`[:_&gt;^GT^`X"^0_*XRZ?KWQ@RJ1^XD@P]?W^^&gt;^*`0%4G*,W)E#.$!;GE5(2]RX&gt;]RX&gt;]RT&gt;]QT&gt;]QT&gt;]QQ`Y!4`A"`S!(`!68`%68`%68`(TA3`YAC`Y\)L4X'HE&amp;(5+/*."%?@$Y8&amp;Y("[(B[`C]$A]$I`$Y`!Q22Q?B]@B=8A=(N,%Y8&amp;Y("[(R_'B6#&lt;R@/$$Y`"18AQ?A]@A-8A-(FK+Q7-!'-W-QE92--1)'B='D]&amp;D](!J"I`"9`!90!9094&amp;Y$"[$R_!R?%D*OW+:*BXY]&amp;"'!DQ"HA"0A#@!1WE*]!2Y!DQ"HA!0\34!%_!*1!1;"IK$))'EQ)4!FQ"0A)?4"(A#0!'?!%_!BV#_1S(P4+**"TY]#I`#I`!I0!I0*54B58A5(I6(Y;'M+$Q+D]+D]#A]N"+&amp;2_&amp;2?"11J3HN23GG*#K4&amp;%0BY3]`,:LPEG53T9&gt;)?8CF0*23(D9J$Z'5BU0+4:&gt;S-[8=*#G&lt;,W64J7S7F%W1MDAJU&amp;*AJ$32EJQGSB7@&amp;[C0^F%8L;-G7E',;#'F`MM4L[[OZ/,C1PL^POTP\UOXWZ8V^86J.JOSML)CCYO,ML#Q=0.TYB?/W:C\@C^N=9U?I)&gt;*QTC[@"G@8$Y@JP.](-\GX-4\BTO8X@L*)RPM4,PV`/.B@0$J/H9@P;4?]/;^^(^Y.]K]R'^_BL&amp;(8Q"0=[&gt;P!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">318799872</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="apply new settings.vi" Type="VI" URL="../VIs/apply new settings.vi"/>
	<Item Name="dev init.vi" Type="VI" URL="../VIs/dev init.vi"/>
	<Item Name="EEMP_HAL.dll" Type="Document" URL="../EEMP_HAL.dll"/>
	<Item Name="fpga global reset.vi" Type="VI" URL="../VIs/fpga global reset.vi"/>
	<Item Name="fpga global rst active.vi" Type="VI" URL="../VIs/fpga global rst active.vi"/>
	<Item Name="fpga global rst inactive.vi" Type="VI" URL="../VIs/fpga global rst inactive.vi"/>
	<Item Name="get endpoint status.vi" Type="VI" URL="../VIs/get endpoint status.vi"/>
	<Item Name="get ep2 usbd status string.vi" Type="VI" URL="../VIs/get ep2 usbd status string.vi"/>
	<Item Name="get power status.vi" Type="VI" URL="../VIs/get power status.vi"/>
	<Item Name="get soc status.vi" Type="VI" URL="../VIs/get soc status.vi"/>
	<Item Name="power on off circuits.vi" Type="VI" URL="../VIs/power on off circuits.vi"/>
	<Item Name="read ep2 data.vi" Type="VI" URL="../VIs/read ep2 data.vi"/>
	<Item Name="read pkt header.vi" Type="VI" URL="../VIs/read pkt header.vi"/>
	<Item Name="reset ep2 buffer.vi" Type="VI" URL="../VIs/reset ep2 buffer.vi"/>
	<Item Name="reset ep8 buffer.vi" Type="VI" URL="../VIs/reset ep8 buffer.vi"/>
	<Item Name="set acdc.vi" Type="VI" URL="../VIs/set acdc.vi"/>
	<Item Name="set attenuation.vi" Type="VI" URL="../VIs/set attenuation.vi"/>
	<Item Name="set auto trigger.vi" Type="VI" URL="../VIs/set auto trigger.vi"/>
	<Item Name="set clk div factor.vi" Type="VI" URL="../VIs/set clk div factor.vi"/>
	<Item Name="set op mode.vi" Type="VI" URL="../VIs/set op mode.vi"/>
	<Item Name="set sig gen dac config.vi" Type="VI" URL="../VIs/set sig gen dac config.vi"/>
	<Item Name="set sig gen dds config.vi" Type="VI" URL="../VIs/set sig gen dds config.vi"/>
	<Item Name="set sig gen pio.vi" Type="VI" URL="../VIs/set sig gen pio.vi"/>
	<Item Name="set trigger mode.vi" Type="VI" URL="../VIs/set trigger mode.vi"/>
	<Item Name="set trigger position.vi" Type="VI" URL="../VIs/set trigger position.vi"/>
	<Item Name="set trigger source.vi" Type="VI" URL="../VIs/set trigger source.vi"/>
	<Item Name="set trigger voltage level.vi" Type="VI" URL="../VIs/set trigger voltage level.vi"/>
	<Item Name="set vga config.vi" Type="VI" URL="../VIs/set vga config.vi"/>
</Library>
