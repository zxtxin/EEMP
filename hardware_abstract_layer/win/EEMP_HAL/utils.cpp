#include "stdafx.h"
#include <intrin.h>
int count_leading_zeros(unsigned int x)
{
	unsigned long index;
	unsigned char isNonzero = _BitScanReverse(&index, x);
	return isNonzero ? index : 0;
}
int count_tailing_zeros(unsigned int x)
{
	unsigned long index;
	unsigned char isNonzero = _BitScanForward(&index, x);
	return isNonzero ? index : 0;
}