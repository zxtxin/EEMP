#ifndef EEMP_HAL_H_
#define EEMP_HAL_H_
#include <stdint.h>
#ifdef API_EXPORT
#define API __declspec(dllexport)
#else
#define API 
#endif

#define VID 0x04B4
#define PID 0x1004

#define EP2_BUF_SIZE 1024

#define CHA_FIFO_SIZE 8192
#define CHB_FIFO_SIZE 8192
#define LA_FIFO_SIZE 8192



#define bmCFG_INDEX					0xff000000
#define bmCFG_LABEL					0x00ff0000
#define bmCFG_DATA					0x0000ffff
#define bmACDC_COUPLING				0x0003
#define bmATTENUATION				0x003c
#define bmOPERATION_MODE			0x0003
#define bmCLK_DIV_FACTOR			0xfc00
#define bmTRIGGER_MODE				0x0300
#define bmTRIGGER_VOLTAGE_LEVEL		0x00ff
#define bmAUTO_TRIGGER				0x8000
#define bmTRIGGER_SOURCE			0x4000
#define bmTRIGGER_POSITION			0x1fff
#define bmSPI_DATA					0xffff
#ifdef __cplusplus
extern "C"{
#endif
	API bool dev_init();

	API bool apply_new_settings(bool *scope_cfg_update, bool *sig_gen_cfg_update, bool *la_cfg_update);
	API bool read_ep2_data(uint8_t *buf, uint32_t *length);

	API bool power_on_off_circuits();
	API bool get_endpoint_status(uint8_t *stat);
	API bool get_power_status(uint8_t *stat);
	API bool fpga_global_reset();
	API bool fpga_global_rst_active();
	API bool fpga_global_rst_inactive();
	API bool reset_ep2_buffer();
	API bool reset_ep8_buffer();
	API bool get_soc_status(uint8_t *stat);

	API void set_attenuation(uint8_t ch_a,uint8_t ch_b);
	API void set_acdc(uint8_t ch_a, uint8_t ch_b);
	API void set_vga_config(uint8_t ch, uint8_t data);
	API void set_ch_offset(uint8_t ch, uint8_t data);
	API void set_op_mode(uint8_t op_mode);
	API void set_clk_div_factor(uint8_t clk_div_factor);
	API void set_trigger_mode(uint8_t trigger_mode);
	API void set_trigger_voltage_level(uint8_t trigger_voltage_level);
	API void set_auto_trigger(uint8_t en);
	API void set_trigger_source(uint8_t trigger_source);
	API void set_trigger_position(uint16_t trigger_position);

	API void set_sig_gen_dds_config(uint16_t data,uint8_t seq);
	API void set_sig_gen_dac_config(uint16_t data);
	API void set_sig_gen_pio(uint16_t data);

	API uint32_t get_ep2_usbd_status_string(char *s);
#ifdef __cplusplus
}
#endif
#endif