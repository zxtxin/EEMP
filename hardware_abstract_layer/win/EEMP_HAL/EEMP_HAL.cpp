// EEMP_HAL.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include "CyAPI.h"
#include <stdint.h>
#include "utils.h"
#define API_EXPORT
#include "EEMP_HAL.h"
#include "req_code.h"
#include "eemp_common.h"
#define SET_FIELD(FIELD,VAL,MASK)							\
	do{														\
			(FIELD)&=~(MASK);								\
			(FIELD)|=(VAL)<<count_tailing_zeros((MASK));		\
	}while(0)
#define EP8_FULL_MASK 0x80
#define CFG_BUF_SIZE  20

typedef struct {
	uint16_t label[6];
	bool flag[6];
}scope_cfg_t;
typedef struct {
	uint16_t label[5];
	bool flag[5];
}logic_analyzer_cfg_t;
typedef struct {
	uint16_t label[7];
	bool flag[7];
}sig_gen_cfg_t;
typedef struct {
	scope_cfg_t scope_cfg;
	logic_analyzer_cfg_t la_cfg;
	sig_gen_cfg_t sig_gen_cfg;
}pending_cfg_t;

typedef struct{
	CCyUSBDevice *USBDevice;
	CCyControlEndPoint *CtrlEpt;
}cy_usb_instant_t;
pending_cfg_t pending_cfg;
cy_usb_instant_t cy_usb;


static void ctrl_endpoint_init()
{
	cy_usb.CtrlEpt = cy_usb.USBDevice->ControlEndPt;
	cy_usb.CtrlEpt->Target = TGT_DEVICE;
	cy_usb.CtrlEpt->ReqType = REQ_VENDOR;
	cy_usb.CtrlEpt->Value = 1;
	cy_usb.CtrlEpt->Index = 0;
}

bool dev_init()
{
	uint16_t vID, pID;
	uint8_t devices,d;
	cy_usb.USBDevice = new  CCyUSBDevice(NULL);
	devices = cy_usb.USBDevice->DeviceCount();
	for (d = 0; d < devices; d++) 
	{
		cy_usb.USBDevice->Open(d);   // Open automatically  calls Close(  ) if necessary
		vID = cy_usb.USBDevice->VendorID;
		pID = cy_usb.USBDevice->ProductID;
		if (vID == VID&&pID == PID)
		{
			break;
		}
	}
	if (d == devices)
	{
		return false;
	}else {
		ctrl_endpoint_init();
		return true;
	}
}

uint32_t get_ep2_usbd_status_string(char *s)
{
	cy_usb.USBDevice->UsbdStatusString(cy_usb.USBDevice->BulkInEndPt->UsbdStatus,s);
	return cy_usb.USBDevice->NtStatus;
}
static uint32_t *scope_fill_config_buf(uint32_t *buf)
{
	scope_cfg_t *scope_cfg_ptr = &pending_cfg.scope_cfg;
	uint32_t cfg_data;
	uint8_t i;
	SET_FIELD(cfg_data, SCOPE_CFG_INDEX, bmCFG_INDEX);
	for (i = 0; i < sizeof(scope_cfg_ptr->label) / sizeof(scope_cfg_ptr->label[0]); i++)
	{
		SET_FIELD(cfg_data, i, bmCFG_LABEL);
		if (scope_cfg_ptr->flag[i])
		{
			scope_cfg_ptr->flag[i] = false;
			SET_FIELD(cfg_data, scope_cfg_ptr->label[i], bmCFG_DATA);
			*buf++ = cfg_data;
		}
	}
	return buf;
}
static uint32_t *la_fill_config_buf(uint32_t *buf)
{
	logic_analyzer_cfg_t *la_cfg_ptr = &pending_cfg.la_cfg;
	uint32_t cfg_data;
	uint8_t i;
	SET_FIELD(cfg_data, LA_CFG_INDEX, bmCFG_INDEX);
	for (i = 0; i < sizeof(la_cfg_ptr->label) / sizeof(la_cfg_ptr->label[0]); i++)
	{
		SET_FIELD(cfg_data, i, bmCFG_LABEL);
		if (la_cfg_ptr->flag[i])
		{
			la_cfg_ptr->flag[i] = false;
			SET_FIELD(cfg_data, la_cfg_ptr->label[i], bmCFG_DATA);
			*buf++ = cfg_data;
		}
	}
	return buf;
}
static uint32_t *sig_gen_fill_config_buf(uint32_t *buf)
{
	sig_gen_cfg_t *sig_gen_cfg_ptr = &pending_cfg.sig_gen_cfg;
	uint32_t cfg_data;
	uint8_t i;
	SET_FIELD(cfg_data, SIG_GEN_CFG_INDEX, bmCFG_INDEX);
	for (i = 0; i < sizeof(sig_gen_cfg_ptr->label) / sizeof(sig_gen_cfg_ptr->label[0]); i++)
	{
		SET_FIELD(cfg_data, i , bmCFG_LABEL);
		if (sig_gen_cfg_ptr->flag[i])
		{
			sig_gen_cfg_ptr->flag[i] = false;
			SET_FIELD(cfg_data, sig_gen_cfg_ptr->label[i], bmCFG_DATA);
			*buf++ = cfg_data;
		}
	}
	return buf;
}

bool apply_new_settings(bool *scope_cfg_update,bool *sig_gen_cfg_update,bool *la_cfg_update)
{
	uint32_t tx_buf[CFG_BUF_SIZE];
	uint32_t *nx_ptr, *pr_ptr = tx_buf;
	nx_ptr = scope_fill_config_buf(pr_ptr);
	*scope_cfg_update = nx_ptr == pr_ptr ? false : true;
	pr_ptr = nx_ptr;

	nx_ptr = sig_gen_fill_config_buf(pr_ptr);
	*sig_gen_cfg_update = nx_ptr == pr_ptr ? false : true;
	pr_ptr = nx_ptr;

	nx_ptr = la_fill_config_buf(pr_ptr);
	*la_cfg_update = nx_ptr == pr_ptr ? false : true;
	pr_ptr = nx_ptr;
	
	LONG packet_size = (nx_ptr - tx_buf)*sizeof(uint32_t);
	if (*scope_cfg_update | *sig_gen_cfg_update | *la_cfg_update) {
		return cy_usb.USBDevice->BulkOutEndPt->XferData((uint8_t *)tx_buf, packet_size);
	}
	else{
		return true;
	}

}

bool read_ep2_data(uint8_t *buf, uint32_t *length)
{
	LONG size = *length;
	bool ret = cy_usb.USBDevice->BulkInEndPt->XferData(buf, size);
	*length = size;
	return ret;
}

bool power_on_off_circuits()
{
	LONG length = 0;
	cy_usb.CtrlEpt->ReqCode = POWER_ON_OFF;
	return cy_usb.CtrlEpt->Write(NULL, length);
}
bool get_endpoint_status(uint8_t *stat)
{
	LONG length = sizeof(*stat);
	cy_usb.CtrlEpt->ReqCode = GET_EPSTAT;
	return cy_usb.CtrlEpt->Read(stat, length);
}
bool get_power_status(uint8_t *stat)
{
	LONG length = sizeof(*stat);
	cy_usb.CtrlEpt->ReqCode = GET_POWER_STATUS;
	return cy_usb.CtrlEpt->Read(stat, length);
}
bool fpga_global_reset()
{
	LONG length = 0;
	cy_usb.CtrlEpt->ReqCode = FPGA_GLOBAL_RST;
	return cy_usb.CtrlEpt->Write(NULL, length);
}
bool fpga_global_rst_active()
{
	LONG length = 0;
	cy_usb.CtrlEpt->ReqCode = FPGA_GLOBAL_RST_ACTIVE;
	return cy_usb.CtrlEpt->Write(NULL, length);
}
bool fpga_global_rst_inactive()
{
	LONG length = 0;
	cy_usb.CtrlEpt->ReqCode = FPGA_GLOBAL_RST_INACTIVE;
	return cy_usb.CtrlEpt->Write(NULL, length);
}
bool reset_ep2_buffer()
{
	LONG length = 0;
	cy_usb.CtrlEpt->ReqCode = RST_EP2_BUF;
	return cy_usb.CtrlEpt->Write(NULL, length);
}
bool reset_ep8_buffer()
{
	LONG length = 0;
	cy_usb.CtrlEpt->ReqCode = RST_EP8_BUF;
	return cy_usb.CtrlEpt->Write(NULL, length);
}
bool get_soc_status(uint8_t *stat)
{
	LONG length = sizeof(*stat);
	cy_usb.CtrlEpt->ReqCode = GET_SOC_STAT;
	return cy_usb.CtrlEpt->Read(stat, length);
}

void set_attenuation(uint8_t ch_a, uint8_t ch_b)
{
	uint16_t val = ch_a | (ch_b<<2);
	SET_FIELD(pending_cfg.scope_cfg.label[3], val, bmATTENUATION);
	pending_cfg.scope_cfg.flag[3] = true;
}
void set_acdc(uint8_t ch_a, uint8_t ch_b)
{
	uint16_t val = ch_a | (ch_b<<1);
	SET_FIELD(pending_cfg.scope_cfg.label[3], val, bmACDC_COUPLING);
	pending_cfg.scope_cfg.flag[3] = true;
}
void set_vga_config(uint8_t ch,uint8_t data)
{
	uint16_t val = 0x1000;
	val |= ((ch << 14)|(data<<4));
	SET_FIELD(pending_cfg.scope_cfg.label[4], val, bmSPI_DATA);
	pending_cfg.scope_cfg.flag[4] = true;
}
void set_ch_offset(uint8_t ch, uint8_t data)
{
	uint16_t val = 0x9000;
	val |= ((ch << 14) | (data << 4));
	SET_FIELD(pending_cfg.scope_cfg.label[4], val, bmSPI_DATA);
	pending_cfg.scope_cfg.flag[4] = true;
}
void set_op_mode(uint8_t op_mode)
{
	uint16_t val = op_mode;
	SET_FIELD(pending_cfg.scope_cfg.label[2], val, bmOPERATION_MODE);
	pending_cfg.scope_cfg.flag[2] = true;

}
void set_clk_div_factor(uint8_t clk_div_factor)
{
	uint16_t val = clk_div_factor;
	SET_FIELD(pending_cfg.scope_cfg.label[1], val, bmCLK_DIV_FACTOR);
	pending_cfg.scope_cfg.flag[1] = true;
}
void set_trigger_mode(uint8_t trigger_mode)
{
	uint16_t val = trigger_mode;
	SET_FIELD(pending_cfg.scope_cfg.label[1], val, bmTRIGGER_MODE);
	pending_cfg.scope_cfg.flag[1] = true;
}
void set_trigger_voltage_level(uint8_t trigger_voltage_level)
{
	uint16_t val = trigger_voltage_level;
	SET_FIELD(pending_cfg.scope_cfg.label[1], val, bmTRIGGER_VOLTAGE_LEVEL);
	pending_cfg.scope_cfg.flag[1] = true;
}
void set_auto_trigger(uint8_t en)
{
	uint16_t val = en;
	SET_FIELD(pending_cfg.scope_cfg.label[0], val, bmAUTO_TRIGGER);
	pending_cfg.scope_cfg.flag[0] = true;
}
void set_trigger_source(uint8_t trigger_source)
{
	uint16_t val = trigger_source;
	SET_FIELD(pending_cfg.scope_cfg.label[0], val, bmTRIGGER_SOURCE);
	pending_cfg.scope_cfg.flag[0] = true;
}
void set_trigger_position(uint16_t trigger_position)
{
	uint16_t val = trigger_position;
	SET_FIELD(pending_cfg.scope_cfg.label[0], val, bmTRIGGER_POSITION);
	pending_cfg.scope_cfg.flag[0] = true;
}
void set_sig_gen_dds_config(uint16_t data,uint8_t seq)
{
	uint16_t val = data;
	SET_FIELD(pending_cfg.sig_gen_cfg.label[seq], val, bmSPI_DATA);
	pending_cfg.sig_gen_cfg.flag[seq] = true;
}
void set_sig_gen_dac_config(uint16_t data)
{
	uint16_t val = data;
	SET_FIELD(pending_cfg.sig_gen_cfg.label[5], val, bmSPI_DATA);
	pending_cfg.sig_gen_cfg.flag[5] = true;
}
void set_sig_gen_pio(uint16_t data)
{
	uint16_t val = data;
	SET_FIELD(pending_cfg.sig_gen_cfg.label[6], val, bmCFG_DATA);
	pending_cfg.sig_gen_cfg.flag[6] = true;
}