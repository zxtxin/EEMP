#ifndef UTILS_H_
#define UTILS_H_
int count_leading_zeros(unsigned int x);
int count_tailing_zeros(unsigned int x);
#endif